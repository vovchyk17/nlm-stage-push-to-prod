// copy text to clipboard
function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;
    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';
    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;
    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();
    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }
    document.body.removeChild(textArea);
}
function CopyLink() {
    alert('Link copied!');
    copyTextToClipboard(location.href);
}

$(document).ready(function() {
    'use strict';
// side_nav
//     if($('.tpl-about').length > 0) {
//         var sections_holder = $('.about_content');
//         var side_nav = [];
//         var find_row = $('.about_content__row');
//         sections_holder.find(find_row).each(function () {
//             if ($(this).attr('id') != null ) {
//                 var id = $(this).attr('id'),
//                     title_li = id.replace('_', ' ').replace('_', ' '),
//                     title = title_li.replace('and', '&'),
//                     anchor = "<li><a href='#to-"+id+"'>"+title+"</a></li>";
//                 side_nav.push(anchor);
//             }
//         });
//         $('.about_sticky_nav ol').append(side_nav).find('li:first-child').addClass('is_active');
//
//         // scroll + click init
//         // click_nav();
//     }

    scroll_nav();


    $('.home_intro .toggle_btn').on('click', function () {
        $('.home_intro .content').toggle();
        $(this).toggleClass('is_active');
        return false;
    });

    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 5) {
            $('.about_sticky_nav').addClass('is_sticky');
        } else {
            $('.about_sticky_nav').removeClass('is_sticky');
        }

        var single_essay_content = $('.single_photo_essay__main_content');

        if(single_essay_content.length > 0) {
            if ($(this).scrollTop() > (single_essay_content.offset().top + 80)) {
                $('.single_photo_essay__parallax_wrapper').addClass('hidden');
            } else {
                $('.single_photo_essay__parallax_wrapper').removeClass('hidden');
            }

            if ($(this).scrollTop() > (single_essay_content.offset().top + 50)) {
                $('header').addClass('shown');
            } else {
                $('header').removeClass('shown');
            }
        }

    });
    if ($(this).scrollTop() > 5) $('.about_sticky_nav').addClass('is_sticky');

    //New Lines Search toggle
    $('.search_toggle').on('click', function () {
        $(this).toggleClass('is_shown');
        $('.search_form').toggleClass('is_shown');
    });

    $('.popular_post').each(function () {
        if ($(this).outerHeight() > 160) {
            var new_height = $(this).outerHeight();
            $('.popular_post').css('min-height', new_height);
        }
    });

    function acc_action(elem) {
        let exp = elem.attr('aria-expanded');
        let hid = elem.next().attr('aria-hidden');
        (exp === 'false') ? elem.attr('aria-expanded', 'true') : elem.attr('aria-expanded', 'false');
        (hid === 'true') ? elem.next().attr('aria-hidden', 'false') : elem.next().attr('aria-hidden', 'true');
        elem.toggleClass('is_open').next().toggle();
        elem.find('.circle_arrow').toggleClass('is_up').toggleClass('is_down');
    }
    $('.acc_title').on('click', function () {
        acc_action($(this));
    }).on('keyup', function (e) {
        if (e.keyCode === 13) {
            acc_action($(this));
        }
    });

    $('.newsletter_checkboxes .wpcf7-checkbox').append('<span class="wpcf7-list-item"><a href="javascript:;" class="nwsltr_toggle_all is_selected">Select all</a></span>');

    // toggle all checkboxes
    $('.nwsltr_toggle_all').on('click', function () {
        $(this).toggleClass('is_selected');
        $('.newsletter_checkboxes input[type="checkbox"] + span').each(function () {
            $(this).click();
        });
    });

    $('.sign_up_toggle_all__button').on('click', function () {
        $('.email-lists input[type="checkbox"] + span').each(function () {
            $(this).click();
        });
    });

    $('.nwsltr_checkbox_desc_1').appendTo('.newsletter_checkboxes .wpcf7-list-item:first-child');
    $('.nwsltr_checkbox_desc_2').appendTo('.newsletter_checkboxes .wpcf7-list-item:nth-child(2)');
    $('.nwsltr_checkbox_desc_3').appendTo('.newsletter_checkboxes .wpcf7-list-item:nth-child(3)');
    $('.nwsltr_checkbox_desc_4').appendTo('.newsletter_checkboxes .wpcf7-list-item:nth-child(4)');
    $('.nwsltr_checkbox_desc_5').appendTo('.newsletter_checkboxes .wpcf7-list-item:nth-child(5)');

});

$(window).on('load', function() {
    'use strict';

    // swiper
    setTimeout(function() {
        $('.team_mob_slider').each(function () {
            var t = $(this);
            var program_team_slider = new Swiper(t.find('.swiper-container'), {
                pagination: {
                    el: $('.sw_pagination', t),
                    dynamicBullets: true,
                    clickable: true
                },
                slidesPerView: 'auto',
                spaceBetween: 30,
                speed: 600
            });
        });
    }, 250);

    // scroll to section on hash
    if ($('.tpl-about').length > 0) {
        setTimeout(function() {
            var hash = window.location.hash;
            if(hash && hash !== '#/') {
                $('html,body').animate({
                    scrollTop: $(hash.replace('#/', '#')).offset().top - 79
                }, 500);
            }
        }, 250);
    }

});

// if($('.tpl-about').length > 0) {
    // side_nav scroll actions
    function scroll_nav() {
        var lastId, id,
            topMenu = $('.single_photo_essay__parallax_nav'),
            menuItems = topMenu.find('a'),
            scrollItems = menuItems.map(function(){
                if(this.href.indexOf('#') !== -1) {
                    var item = $($(this).attr('href').replace('to-', ''));
                    if (item.length) { return item; }
                }
            });
        $(window).scroll(function() {
            var $window = $(window),
                ww = $window.width(),
                fromTop = $(this).scrollTop(),
                cur = scrollItems.map(function(){
                    if (($(this).offset().top - 70) < fromTop) return this;
                });
            cur = cur[cur.length-1];
            var id = cur && cur.length ? cur[0].id : '';

            // console.log(cur[0].nextSibling);

            if (lastId !== id) {
                lastId = id;
                menuItems.parent().removeClass('is_active').end().filter('[href="#to-' + id + '"]').parent().addClass('is_active');

                // console.log(menuItems.length);
                var i;
                for (i = 1; i < menuItems.length + 1; i++) {
                    if (id === 'p_text_'+i) {
                        $('.single_photo_essay__parallax_image').removeClass('is_active');
                        $('#p_img_'+i).addClass('is_active');
                    }
                }

            }

        });
    }

// side_nav click actions
//     function click_nav() {
//         $('.about_sticky_nav a[href^="#"]').on('click', function () {
//             var id = $(this).attr('href').replace('to-', ''),
//                 $window = $(window),
//                 ww = $window.width();
//
//             $('html,body').stop().animate({
//                 scrollTop: $(id).offset().top - (ww>1024?77:149)
//             }, 700);
//
//             window.location.hash = '/'+id.replace('#', '');
//
//             // $('.nav_icon.is_active').click();
//
//             return false;
//         });
//     }
// }