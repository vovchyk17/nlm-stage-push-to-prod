/*jslint browser: true, white: true, plusplus: true, regexp: true, indent: 4, maxerr: 50, es5: true */
/*jshint multistr: true, latedef: nofunc */
/*global jQuery, $, Swiper*/


// ajax posts loading
function load_posts_ajax(paged, taxonomy, term) {
    if(!paged) {
        paged = 1;
    }

    var ajax_content = $('.articles__container');

    $.ajax({
        type: 'POST',
        url: $('body').data('a'),
        data: {
            action: 'load_posts_ajax',
            paged: paged,
            taxonomy: taxonomy,
            term: term
        },
        success: function (html) {
            $('.loader_holder').remove();

            if (paged !== 1) {
                ajax_content.append(html);
            } else {
                ajax_content.html(html);
            }

            $('.show_box').removeClass('is_loading');
        }

    });

    return false;
}



// ajax photo_essays loading
function photo_essays_ajax(paged) {
    if(!paged) {
        paged = 1;
    }

    var ajax_content = $('.photo_essays__container');

    $.ajax({
        type: 'POST',
        url: $('body').data('a'),
        data: {
            action: 'photo_essays_ajax',
            paged: paged,
        },
        success: function (html) {
            $('.loader_holder').remove();

            if (paged !== 1) {
                ajax_content.append(html);
            } else {
                ajax_content.html(html);
            }

            $('.show_box').removeClass('is_loading');
        }

    });

    return false;
}



$(document).ready(function () {
    'use strict';

    // ajax posts - page loading
    $(this).on('click', '.load_more_posts', function () {
        $(this).parent().next().find('.show_box').addClass('is_loading');

        var pg = $(this).attr('data-href'),
            tax = $(this).attr('data-tax'),
            term = $(this).attr('data-term');

        load_posts_ajax(pg === 1 ? 2 : pg, tax, term);

        $(this).parent().remove();

        return false;
    });


    // ajax photo_essays - page loading
    $(this).on('click', '.load_more_photo_essays', function () {
        $(this).parent().next().find('.show_box').addClass('is_loading');

        var pg = $(this).attr('data-href');

        photo_essays_ajax(pg === 1 ? 2 : pg);

        $(this).parent().remove();

        return false;
    });


    // search page
    $('.search__filters_toggle').on('click', function () {
        $(this).toggleClass('is_show').next().toggle();
    });

    $('.search__filter_row.ptypes input[type=checkbox]').on('change', function(){
        $(this).parents('.search__filter_group').find('.show_box').addClass('is_loading');

        searchAjax();
    });

    $(document).on('click', '.load_more__search', function() {
        $(this).parent().next().find('.show_box').addClass('is_loading');

        var paged = $(this).data('href');
        searchAjax(paged);

        $(this).parent().remove();

        return false;
    });

    // search ajax func
    function searchAjax(paged) {
        if(!paged) {
            paged = 1;
        }

        var post_types = [];

        $('.search__filter_row.ptypes input[type=checkbox]:checked').each(function () {
            post_types.push($(this).data('post_type'));
        });

        var ajax_content = $('.search_results__container'),
            s = $('#s').val();

        $.ajax({
            type: 'POST',
            url: $('body').data('a'),
            data: {
                action: 'load_search_ajax',
                s: s,
                paged: paged,
                post_types: post_types
            },
            success: function (html) {
                $('.loader_holder').remove();

                var result = JSON.parse(html);

                if (paged !== 1) {
                    ajax_content.append(result.html);
                } else {
                    ajax_content.html(result.html);
                }

                $('#foundPosts').html(result.foundPosts);

                $('.show_box').removeClass('is_loading');
            }
        });

        return false;
    }

});