/*jslint browser: true, white: true, plusplus: true, regexp: true, indent: 4, maxerr: 50, es5: true */
/*jshint multistr: true, latedef: nofunc */
/*global jQuery, $, Swiper*/

$(document).ready(function() {
    'use strict';

    // hamburger + menu
    $('.nav_icon').on('click', function() {
        $(this).toggleClass('is_active');
        $('.header__menu').toggleClass('is_open');
        // $('.search_toggle').toggleClass('is_shown');
        $('body').toggleClass('is_overflow');
    });

    // header
    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 5) {
            $('header').addClass('is_sticky');
        } else {
            $('header').removeClass('is_sticky');
        }
    });
    if ($(this).scrollTop() > 5) $('header').addClass('is_sticky');

    // contact form 7
    $(this).on('click', '.wpcf7-not-valid-tip', function() {
        $(this).prev().trigger('focus');
        $(this).fadeOut(250,function(){
            $(this).remove();
        });
    });

    // custom select
    $('select').selectric({
        disableOnMobile: false,
        nativeOnMobile: false,
        arrowButtonMarkup: '<span class="select_arrow"></span>'
    });
    $('select.wpcf7-form-control').each(function () {
        $(this).find('option').first().val('');
    });

    // set maximum length of multiple select label
    $('select.is_multiple').on('change', function () {
        var label = $(this).parent().next().find('.label'),
            label_text = label.text(),
            label_length = label_text.length,
            label_desired;

        if (label_length > 40) {
            label_desired = label_text.slice(0, 40)+'...';
            label.text(label_desired);
        }
    });


    // fancybox
    $.fancybox.defaults.touch = false;
    $.fancybox.defaults.smallBtn = false;
    // $.fancybox.defaults.autoFocus = false;

    $('[data-fancybox]').fancybox({
        afterLoad: function( instance, slide ) {
            // fix if header is sticky
            $('header').addClass('compensate-for-scrollbar');
        },
        afterClose: function( instance, slide ) {
            // fix if header is sticky
            $('header').removeClass('compensate-for-scrollbar');
        }
    });

    // decrease spacing if button goes after
    // $('.content p, .content h4, .content h5, .content ul, .content ol').each(function () {
    //     if($(this).next().hasClass('button')) {
    //         $(this).addClass('no_spacing');
    //     }
    // });

    // clean empty tags from acf two_columns block
    $('.acf_two_columns, .acf_one_column').each(function () {
        $(this).find('p.j_empty').remove();
    });


    // sticky shares
    var sticky_shares = $('.single_post__shrs');
    if (sticky_shares.length > 0) {
        var shares_position = sticky_shares.offset().top - 145;

        if ($(window).scrollTop() > shares_position) {
            sticky_shares.addClass('is_sticky');
        } else {
            sticky_shares.removeClass('is_sticky');
        }

        $(window).on('scroll', function() {
            if ($(window).scrollTop() > shares_position) {
                sticky_shares.addClass('is_sticky');
            } else {
                sticky_shares.removeClass('is_sticky');
            }

            var single_article = $('.single_post__content');
            if($(window).scrollTop() >= single_article.offset().top + single_article.height() - sticky_shares.outerHeight() - 160) {
                sticky_shares.addClass('reached_bottom');
            } else {
                sticky_shares.removeClass('reached_bottom');
            }
        });
    }

    // additional UI
    $('.content p').each(function () {
        if($(this).next().hasClass('wp-block-separator')) {
            $(this).addClass('less_spacing');
        }
    });


    $(this).on('submit', '.search_form form', function (e) {
        $(this).find('.show_box').addClass('is_loading');
    });

});



$(window).on('load', function() {
    'use strict';
    // custom class for video in content (iframe)
    $('.content iframe').each(function(i) {
        var t = $(this),
            p = t.parent();
        if (p.is('p') && !p.hasClass('full_frame')) {
            p.addClass('full_frame');
        }
    });

});
