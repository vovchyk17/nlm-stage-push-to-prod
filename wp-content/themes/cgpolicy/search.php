<?php
get_header();
$i_query = array(
	'paged' => 1,
	's'     => $s,
);
$result = load_search_ajax($i_query);
?>

<section class="search_page container left_spacing">
    <div class="search_top">
        <div class="has_line">
            <h1>Search Results</h1>
        </div>
        <?php
        global $wp_query;
        $total_posts = $result['foundPosts'];
        ?>
        <h3>Showing <strong id="foundPosts"><?php echo esc_html($total_posts); ?></strong> results for <strong>"<?php echo get_search_query(); ?>"</strong></h3>
    </div>

    <div class="search_results">
        <div class="flex_start__rwd">
            <aside>
                <p class="mob_show">Refine your search</p>
                <div class="search__filters_toggle button is_red is_bigger mob_show">
                    <div><span>Show</span><span>Hide</span> filters</div>
                    <small></small>
                </div>
                <div class="search_filters__mob_wrap">
                    <h4>Categories</h4>
                    <div class="search_filters">
                        <form>
                            <input type="hidden" name="action" value="load_search_ajax">
                            <input type="hidden" name="paged" value="1" id="show_page">
                            <div class="search__filter_group">
                                <div class="search__filter_row ptypes <?php echo $result['post_types_count']['post'] == 0 ? "to_hide" : ""; ?>">
                                    <input type="checkbox" id="filter_post" data-post_type="post">
                                    <label for="filter_post">Articles (<span><?php echo esc_html($result['post_types_count']['post']); ?></span>)</label>
                                </div>
                               <!-- <div class="search__filter_row ptypes <?php /*echo $result['post_types_count']['photo_essay'] == 0 ? "to_hide" : ""; */?>">
                                    <input type="checkbox" id="filter_photo_essay" data-post_type="photo_essay">
                                    <label for="filter_photo_essay">Photo essays (<span><?php /*echo esc_html($result['post_types_count']['photo_essay']); */?></span>)</label>
                                </div>-->
                                <div class="search__filter_row ptypes <?php echo $result['post_types_count']['page'] == 0 ? "to_hide" : ""; ?>">
                                    <input type="checkbox" id="filter_page" data-post_type="page">
                                    <label for="filter_page">Pages (<span><?php echo esc_html($result['post_types_count']['page']); ?></span>)</label>
                                </div>
                                <div class="search__filter_row ptypes <?php echo $result['post_types_count']['people'] == 0 ? "to_hide" : ""; ?>">
                                    <input type="checkbox" id="filter_people" data-post_type="people">
                                    <label for="filter_people">Writers (<span><?php echo esc_html($result['post_types_count']['people']); ?></span>)</label>
                                </div>
                                <div class="search__filter_row ptypes <?php echo $result['post_types_count']['careers'] == 0 ? "to_hide" : ""; ?>">
                                    <input type="checkbox" id="filter_careers" data-post_type="careers">
                                    <label for="filter_careers">Careers (<span><?php echo esc_html($result['post_types_count']['careers']); ?></span>)</label>
                                </div>
								<?php echo wp_kses( get_loader(), $GLOBALS['allowed_loader'] ); ?>
                            </div>
                        </form>
                    </div>
                </div>
            </aside>

            <article>
                <div class="search_results__container">
					<?php echo $result['html']; ?>
                </div>
            </article>
        </div>
    </div>
</section>

<?php get_footer(); ?>