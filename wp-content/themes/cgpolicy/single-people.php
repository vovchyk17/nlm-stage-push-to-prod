<?php get_header(); ?>
<section class="single_people">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="container content">
            <figure class="single_people__thumbnail">
                <?php if (has_post_thumbnail()) { ?>
                    <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID), 'people_big')); ?>"
                         alt="<?php the_title(); ?>">
                <?php } else { ?>
                    <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                         alt="<?php the_title(); ?>">
                <?php } ?>
            </figure>
            <div class="single_people__titles">
                <h1><?php the_title(); ?></h1>
                <?php $position = get_field('position'); ?>
                <?php echo $position ? '<h3>' . esc_html($position) . '</h3>' : ''; ?>
            </div>
            <?php if ($so_me = get_field('so_me')) { ?>
                <div class="so_me">
                    <?php foreach ($so_me as $sm) { ?>
                        <a href="<?php echo esc_url($sm['link']); ?>" class="i_<?php echo esc_attr($sm['icon']); ?>"
                           target="_blank" rel="noopener noreferrer"></a>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="single_people__content">
	            <?php the_content(); ?>
            </div>
        </div>
    <?php endwhile; endif; ?>
</section>
<?php
$args = array(
    'posts_per_page' => -1,
    'post_type' => array('post', 'photo_essay'),
    'post_status' => 'publish',
    'meta_key' => 'authors',
    'meta_value' => '"' . get_the_ID() . '"', //quotes to make sure ID 143 does not match ID 1437, 1143 etc
    'meta_compare' => 'LIKE', // should add compare operator to use ACF relation field because it saves data as serialized array
);
$rel_posts = new WP_Query($args);
if ($rel_posts->have_posts()) : ?>
    <section class="single_people__latest container">
        <div class="home_writers_title">
            <h2>Latest from <?php the_title(); ?></h2>
        </div>
        <div class="">
            <?php while ($rel_posts->have_posts()) : $rel_posts->the_post();
                global $post; ?>
                <div class="article_item flex_start__mob">
                    <a class="article_item__thumb" href="<?php the_permalink(); ?>">
			            <?php if (has_post_thumbnail()) { ?>
                            <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID), 'article_medium')); ?>"
                                 alt="<?php the_title(); ?>">
			            <?php } else { ?>
                            <img src="<?php echo esc_url(theme() . '/images/placeholder-dark.png'); ?>" alt="<?php the_title(); ?>">
			            <?php } ?>
                    </a>
                    <div class="article_item__info">
                        <div class="single_post__meta">
				            <?php
				            if (get_post_type($post->ID) == 'post') :
					            get_template_part( 'tpl-parts/cat-tax-group');
				            endif;
				            ?>
                            <time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date('F j. Y'); ?></time>
                        </div>
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <p><?php echo has_excerpt() ? wp_kses_post( get_the_excerpt() ) : wp_kses_post( wp_trim_words( get_the_content(), 20 ) ); ?></p>
			            <?php if ( $authors = get_field( 'authors' ) ) : ?>
                            <div class="article_item__authors">
					            <?php
					            $a_c = count($authors); $a_i = 1;
					            foreach ( $authors as $key => $value ) :
						            $status = get_post_status( $value );?>
                                    <div class="article_item__author">
							            <?php if ($status === 'publish') : ?>
                                            <a href="<?php echo esc_url(get_permalink($value)); ?>">
									            <?php
									            echo esc_html(get_the_title($value));
									            echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
									            ?>
                                            </a>
							            <?php elseif($status === 'private') : ?>
                                            <span>
                                <?php
                                echo str_replace( 'Private: ', '', get_the_title($value));
                                echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                ?>
                            </span>
							            <? endif; ?>
                                    </div>
					            <?php endforeach; ?>
                            </div>
			            <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </section>
<?php endif;
wp_reset_query(); ?>

<?php get_footer(); ?>
