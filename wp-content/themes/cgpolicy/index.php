<?php
get_header();

if ( is_home() ) :

	$query_name = 'Latest';

    $i_query = array(
	    'paged'    => 1,
	    'taxonomy' => '',
	    'term'     => ''
	);

	$top_args = array(
		'posts_per_page' => 2,
		'post_type'      => 'post',
		'post_status'    => 'publish',
	);

elseif ( is_category() || is_tax( 'analysis') || is_tag() ) :

	$query_name = single_term_title('', false);

	$query_tax = get_queried_object()->taxonomy;
	$query_slug = get_queried_object()->slug;

	$i_query = array(
		'paged'    => 1,
		'taxonomy' => $query_tax,
		'term'     => $query_slug,
	);

	$top_args = array(
		'posts_per_page' => 2,
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => $query_tax,
				'field'    => 'slug',
				'terms'    => array( $query_slug ),
			),
		),
	);

elseif (is_tax( 'photo_essay_cat')) :
	$query_name = single_term_title('', false);

	$query_tax = get_queried_object()->taxonomy;
	$query_slug = get_queried_object()->slug;

	$i_query = array(
		'paged'    => 1,
		'taxonomy' => $query_tax,
		'term'     => $query_slug,
	);

	$top_args = array(
		'posts_per_page' => 2,
		'post_type'      => 'photo_essay',
		'post_status'    => 'publish',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => $query_tax,
				'field'    => 'slug',
				'terms'    => array( $query_slug ),
			),
		),
	);
endif;

?>

<section class="container">
    <div class="articles__title">
        <h1><?php echo esc_html($query_name); ?></h1>
    </div>

    <div class="articles__wrap">
        <div class="articles__top flex_start__rwd">
		    <?php
		    $top_posts = new WP_Query( $top_args );
		    if($top_posts->have_posts()) : while ( $top_posts->have_posts() ) : $top_posts->the_post(); ?>
                <div class="articles__top_item">
                    <a href="<?php the_permalink(); ?>">
					    <?php if (has_post_thumbnail()) : ?>
                            <picture>
                                <source media="(min-width: 640px) and (max-width: 1024px)" srcset="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID), 'full')); ?>">
                                <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID), 'article_big')); ?>" alt="<?php the_title(); ?>">
                            </picture>
					    <?php else : ?>
                            <img src="<?php echo esc_url(theme() . '/images/placeholder-dark.png'); ?>" alt="<?php the_title(); ?>">
					    <?php endif; ?>
                    </a>
                    <div class="single_post__meta">
    				    <?php /*if (get_post_type($post->ID) == 'post') :
	                        get_template_part( 'tpl-parts/cat-tax-group');
                        endif; */?>
	                    <?php get_template_part( 'tpl-parts/cat-tax-group'); ?>
                        <time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date('F j. Y'); ?></time>
                    </div>
                    <h2><a href="<?php the_permalink(); ?>"><?php echo get_field('alt_title') ? esc_html(get_field('alt_title')) : the_title(); ?></a></h2>
                    <p><?php echo has_excerpt() ? wp_kses_post( get_the_excerpt() ) : wp_kses_post( wp_trim_words( get_the_content(), 30 ) ); ?></p>
				    <?php if ( $authors = get_field( 'authors' ) ) : ?>
                        <div class="articles__top_authors">
						    <?php
						    $a_c = count($authors); $a_i = 1;
                            foreach ( $authors as $key => $value ) :
							    $status = get_post_status( $value );?>
                                <div class="articles__top_author">
								    <?php if ($status === 'publish') : ?>
                                        <a href="<?php echo esc_url(get_permalink($value)); ?>">
										    <?php
                                            echo esc_html(get_the_title($value));
										    echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                            ?>
                                        </a>
								    <?php elseif($status === 'private') : ?>
                                        <span>
                                            <?php
                                            echo str_replace( 'Private: ', '', get_the_title($value));
                                            echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                            ?>
                                        </span>
								    <?php endif; ?>
                                </div>
						    <?php endforeach; ?>
                        </div>
				    <?php endif; ?>
                </div>
		    <?php endwhile; endif; ?>
        </div>

        <div class="articles__container">
		    <?php load_posts_ajax($i_query); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>