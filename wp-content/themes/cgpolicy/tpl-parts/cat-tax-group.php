<div class="cat_tax_wrap">
	<?php
	$cat_taxs = get_the_terms($post->ID, 'category');
	if ( !empty($cat_taxs) && !is_wp_error($cat_taxs) ) : ?>
		<div class="single_post__cats">
			<?php echo wp_kses_post(full_cats($post->ID)); ?>
		</div>
	<?php endif; ?>
	<?php
	$photo_taxs = get_the_terms($post->ID, 'photo_essay_cat');
	if ( !empty($photo_taxs) && !is_wp_error($photo_taxs) ) : ?>
        <div class="single_post__cats">
			<?php echo wp_kses_post(custom_tax( $post->ID, 'photo_essay_cat' )); ?>
        </div>
	<?php endif; ?>
	<?php
	$analysis_taxs = get_the_terms($post->ID, 'analysis');
	if ( !empty($analysis_taxs) && !is_wp_error($analysis_taxs) ) : ?>
		<div class="single_post__tax">
			<?php echo wp_kses_post(custom_tax( $post->ID, 'analysis' )); ?>
		</div>
	<?php endif; ?>
</div>