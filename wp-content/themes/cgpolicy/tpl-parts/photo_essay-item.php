<div class="article_item flex_start__rwd">
	<a class="article_item__thumb" href="<?php the_permalink(); ?>">
		<?php if (has_post_thumbnail()) { ?>
			<img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID), 'article_middle')); ?>"
			     alt="<?php the_title(); ?>">
		<?php } else { ?>
			<img src="<?php echo esc_url(theme() . '/images/placeholder-dark.png'); ?>" alt="<?php the_title(); ?>">
		<?php } ?>
	</a>
	<div class="article_item__info">
        <div class="single_post__meta">
	        <?php get_template_part( 'tpl-parts/cat-tax-group'); ?>
            <time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date('F j. Y'); ?></time>
        </div>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <p><?php echo has_excerpt() ? wp_kses_post( get_the_excerpt() ) : wp_kses_post( wp_trim_words( get_the_content(), 50 ) ); ?></p>
		<?php if ( $authors = get_field( 'authors' ) ) : ?>
			<div class="article_item__authors">
				<?php
				$a_c = count($authors); $a_i = 1;
                foreach ( $authors as $key => $value ) :
					$status = get_post_status( $value );?>
					<div class="article_item__author">
						<?php if ($status === 'publish') : ?>
							<a href="<?php echo esc_url(get_permalink($value)); ?>">
                                <?php
                                echo esc_html(get_the_title($value));
                                echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                ?>
                            </a>
						<?php elseif($status === 'private') : ?>
							<span>
                                <?php
                                echo str_replace( 'Private: ', '', get_the_title($value));
                                echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                ?>
                            </span>
						<? endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>