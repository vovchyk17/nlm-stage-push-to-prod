<?php if ( $authors = get_field( 'authors' ) ) : ?>
    <div class="post__authors">
        <?php
        $a_c = count( $authors );
        $a_i = 1;
        foreach ( $authors as $key => $value ) :
            $status = get_post_status( $value ); ?>
            <div class="post__author">
                <?php if ( $status === 'publish' ) { ?>
                    <a href="<?php echo esc_url( get_permalink( $value ) ); ?>">
                        <?php
                        echo esc_html( get_the_title( $value ) );
                        echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                        ?>
                    </a>
                <?php } elseif ( $status === 'private' ) { ?>
                    <span>
                                                <?php
                                                echo str_replace( 'Private: ', '', get_the_title( $value ) );
                                                echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                                ?>
                                            </span>
                <? } ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>