<section class="sign_up_box">
    <div class="container">
        <h2>Sign up to our newsletter</h2>
	    <?php echo do_shortcode( '[contact-form-7 id="19719" title="Sign up"]' ); ?>
        <p>Will be used in accordance with our <a href="/privacy-policy-2">Privacy Policy</a></p>
    </div>
</section>