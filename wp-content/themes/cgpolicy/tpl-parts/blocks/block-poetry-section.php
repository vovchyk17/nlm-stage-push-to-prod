<?php
/**
 * Block Name: Poetry section
 *
 * This is the template that displays the poetry section.
 */

// get fields
$poetry = get_field('poetry');
$p_width = $poetry['text_width'];
$p_position = $poetry['text_position'];
$p_lineheight = $poetry['text_lineheight'];
$p_quotes = $poetry['show_quotes'];
$p_quotes_color = $poetry['quotes_color'];
$p_text = $poetry['text'];

if ($p_width == 'w_75') {
	$width = ' w_75';
} else {
	$width = ' w_100';
}

if ($p_position == 'center') {
	$position = ' center';
} elseif ($p_position == 'right') {
	$position = ' right';
} else {
	$position = '';
}

if ($p_lineheight == 'large') {
	$lh = ' large_lh';
} else {
	$lh = '';
}

if ($p_quotes == true) {
    $quotes = ' has_quotes '. $p_quotes_color;
}

if ($p_text) :?>
    <div class="block__poetry<?php echo $quotes, $position, $width, $lh ?>">
        <?php echo wp_kses_post($p_text) ?>
    </div>
<?php endif; ?>