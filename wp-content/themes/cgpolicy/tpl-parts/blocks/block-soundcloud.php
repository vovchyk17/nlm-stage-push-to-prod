<?php
/**
 * Block Name: Soundcloud
 *
 * This is the template that displays the soundcloud.
 */

// get fields
$s_cloud = get_field('iframe');
$s_cloud_title = get_field('title');

if ($s_cloud) :?>
    <div class="block__soundcloud">
        <?php echo $s_cloud; ?>
        <h5><?php echo esc_html($s_cloud_title) ?></h5>
    </div>
<?php endif; ?>