<?php
/**
 * Block Name: Full image
 *
 * This is the template that displays the full image block.
 */
if( !empty( $block['data']['__is_preview'] ) ) : ?>
    <figure>
        <img src="<?php echo esc_url(theme('images/placeholder-dark.png')); ?>" alt="preview">
    </figure>
<?php return; endif;

// get fields
if (get_field('img')) :
    $img = get_field('img');
    $img_id = $img['id'];
?>
    <div class="block__full_image">
        <figure>
            <img src="<?php echo esc_url(image_src($img_id, 'full')); ?>" alt="<?php echo esc_attr(get_alt($img_id)); ?>">
        </figure>
    </div>
<?php endif; ?>