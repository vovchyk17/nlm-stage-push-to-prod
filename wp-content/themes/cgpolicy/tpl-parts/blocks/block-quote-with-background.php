<?php
/**
 * Block Name: Quote with background
 *
 * This is the template that displays the quote with background.
 */

// get fields
$quote_bg = get_field('quote_bg');
$quote_bg_color = $quote_bg['quotes_color'];
$quote_bg_text = $quote_bg['text'];

if ($quote_bg_text) :?>
    <div class="block__quote_with_bg <?php echo esc_attr($quote_bg_color); ?>">
        <span class="content"><?php echo esc_html($quote_bg_text); ?></span>
    </div>
<?php endif; ?>