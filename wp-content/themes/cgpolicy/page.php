<?php get_header(); ?>

<section class="default_content container left_spacing">
    <div class="default_content__inner">
        <div class="default_content__title">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="content">
            <?php if (have_posts()) : while (have_posts()) : the_post(); the_content(); endwhile; endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
