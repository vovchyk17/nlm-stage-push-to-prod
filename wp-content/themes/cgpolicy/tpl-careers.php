<?php get_header(); /*Template Name: Careers*/ ?>
    <section class="default_content careers_content container">
        <div class="left_spacing">
            <div class="default_content__title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="content">
                <?php if (have_posts()) : while (have_posts()) : the_post();
                    the_content(); endwhile; endif; ?>
            </div>
        </div>
        <?php if(has_post_thumbnail()) { ?>
            <figure class="careers_thumb">
                <img src="<?php echo esc_url(image_src( get_post_thumbnail_id( $post->ID ), 'full' )); ?>" alt="<?php echo esc_attr(get_alt($post->ID)) ?>">
            </figure>
        <?php } ?>
        <?php
        $args = array(
            'post_type' => 'careers',
            'posts_per_page' => '-1'
        );
        $jobs = new WP_Query($args);
        if ($jobs->have_posts()) { ?>
            <div class="jobs_open">
                <h2>Current openings</h2>
                <div class="left_spacing">
                    <div class="flex_grid__mob">
                        <?php while ($jobs->have_posts()) {
                            $jobs->the_post(); ?>
                            <div class="has_line single_position _col_3 __50_rwd">
                                <?php echo '<h4><a href="'.get_the_permalink().'">' . get_the_title() . '</a></h4>'; ?>
                                <?php echo '<p>' . esc_html(get_field('location')) . '</p>'; ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php }
        wp_reset_query(); ?>
    </section>


<?php get_footer(); ?>