<?php get_header(); /*Template Name: About*/ ?>
<?php /*if ($sticky_nav = get_field('content_rows')) { */?><!--
    <div class="about_sticky_nav">
        <ol>
            <li><a href="#to-who_we_are">Who we are</a></li>
        </ol>
    </div>
--><?php /*} */?>
<section id="who_we_are" class="about_main_content container left_spacing">
    <div class="default_content__inner">
        <div class="has_line">
            <?php $custom_title = get_field('custom_title'); ?>
            <?php if ($custom_title) { ?>
                <?php echo $custom_title; ?>
            <?php } else { ?>
                <h1><?php the_title(); ?></h1>
            <?php } ?>
        </div>
        <div class="content">
            <?php if (have_posts()) : while (have_posts()) : the_post();
                the_content(); endwhile; endif; ?>
        </div>
    </div>
</section>
<?php if ($content_rows = get_field('content_rows')) { ?>
    <section class="about_content container content">
        <?php foreach ($content_rows as $row) { ?>
            <div <?php echo $row['anchor'] ? 'id="'. esc_attr($row['anchor']).'"' : ''; ?> class="about_content__row">
                <div class="left_spacing">
                    <div class="has_line">
                        <?php echo $row['title'] ? '<h3>'. esc_html($row['title']) .'</h3>' : ''; ?>
                    </div>
                </div>
                <div class="content left_spacing">
                    <?php echo wp_kses_post($row['text']) ?>
                </div>
                <?php if ($stats = $row['stats']) { ?>
                    <div class="about_content__stats flex_start">
                        <?php foreach ($stats as $s) { ?>
                            <div class="item">
                                <div class="number flex"
                                     style="border-color: <?php echo esc_attr($s['border_color']) ?>;">
                                    <?php echo esc_html($s['number']); ?>
                                </div>
                                <div><?php echo esc_html($s['text']) ?></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php /*if ($programs = $row['programs']) { */?><!--
                    <div class="about_content__programs">
                        <div class="flex_grid">
                            <?php
/*                            foreach ($programs as $program):
                                $thumb = $program['image'];
                                $icon = $program['icon'];
                                $link = $program['link'];
                                */?>
                                <a href="<?php /*echo esc_url($link['url']); */?>"
                                   class="sub_program_grid_item program_grid_item _col_4 __50_rwd __full_mob bg">
                                    <img src="<?php /*echo esc_url($thumb['url']) */?>"
                                         alt="<?php /*echo esc_attr($link['title']); */?>">
                                    <div class="overlay"></div>
                                    <div class="inner">
                                        <span class="icon <?php /*echo esc_attr($icon); */?>"></span>
                                        <h3><?php /*echo esc_html($link['title']) */?></h3>
                                    </div>
                                </a>
                            <?php /*endforeach; */?>
                        </div>
                    </div>
                --><?php /*} */?>
                <?php
                $show = $row['show'];
                if ($show && in_array('team', $show)) { ?>
                    <div class="left_spacing">
                        <?php $terms = get_terms(array(
                            'taxonomy' => 'people_cat',
                            'hide_empty' => true,
                            'orderby' => 'id',
                        )); ?>
                        <?php foreach ($terms as $term) { ?>
                            <div class="about_content__team">
                                <h4><?php echo $term->name; ?></h4>
                                <?php $people = new WP_Query(array(
                                    'post_type' => 'people',
                                    'posts_per_page' => -1,
                                   /* 'order' => 'ASC',
                                    'orderby' => 'title',*/
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'people_cat',
                                            'field' => 'slug',
                                            'terms' => $term,
                                        ),
                                    ),
                                )); ?>
                                <?php if ($people->have_posts()) : ?>
                                    <div class="flex_grid mob_hide">
                                        <?php while ($people->have_posts())  : $people->the_post(); $status = get_post_status();?>
                                            <div class="team_member _col_3 __33_rwd">
                                                <?php if ($status === 'publish') { ?>
                                                    <a href="<?php the_permalink() ?>">
                                                        <figure>
                                                            <?php if (has_post_thumbnail()) { ?>
                                                                <img src="<?php echo esc_url(image_src(get_post_thumbnail_id(), 'people_medium')); ?>"
                                                                     alt="<?php the_title(); ?>">
                                                            <?php } else { ?>
                                                                <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                                                     alt="<?php the_title(); ?>">
                                                            <?php } ?>
                                                        </figure>
                                                        <div class="content">
                                                            <p><span class="tdu"><?php the_title(); ?></span></p>
	                                                        <?php if ( get_field('position') ) : ?>
                                                                <em><?php echo esc_html(get_field('position')); ?></em>
	                                                        <?php endif; ?>
                                                        </div>
                                                    </a>
                                                <?php } elseif($status === 'private') { ?>
                                                    <div>
                                                        <figure>
                                                            <?php if (has_post_thumbnail()) { ?>
                                                                <img src="<?php echo esc_url(image_src(get_post_thumbnail_id(), 'people_medium')); ?>"
                                                                     alt="<?php the_title(); ?>">
                                                            <?php } else { ?>
                                                                <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                                                     alt="<?php the_title(); ?>">
                                                            <?php } ?>
                                                        </figure>
                                                        <div class="content">
                                                            <p><span class="tdu"> <?php echo str_replace( 'Private: ', '', get_the_title()); ?></span></p>
	                                                        <?php if ( get_field('position') ) : ?>
                                                                <em><?php echo esc_html(get_field('position')); ?></em>
	                                                        <?php endif; ?>
                                                        </div>
                                                    </div>
                                                <? } ?>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <div class="team_mob_slider mob_show">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <?php while ($people->have_posts())  : $people->the_post();
                                                    $status = get_post_status();?>
                                                    <div class="team_member swiper-slide">
                                                        <?php if ($status === 'publish') { ?>
                                                            <a href="<?php the_permalink() ?>">
                                                                <figure>
                                                                    <?php if (has_post_thumbnail()) { ?>
                                                                        <img src="<?php echo esc_url(image_src(get_post_thumbnail_id(), 'people_medium')); ?>"
                                                                             alt="<?php the_title(); ?>">
                                                                    <?php } else { ?>
                                                                        <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                                                             alt="<?php the_title(); ?>">
                                                                    <?php } ?>
                                                                </figure>
                                                                <div class="content">
                                                                    <p><span class="tdu"><?php the_title(); ?></span></p>
	                                                                <?php if ( get_field('position') ) : ?>
                                                                        <em><?php echo esc_html(get_field('position')); ?></em>
	                                                                <?php endif; ?>
                                                                </div>
                                                            </a>
                                                        <?php } elseif($status === 'private') { ?>
                                                            <div>
                                                                <figure>
                                                                    <?php if (has_post_thumbnail()) { ?>
                                                                        <img src="<?php echo esc_url(image_src(get_post_thumbnail_id(), 'people_medium')); ?>"
                                                                             alt="<?php the_title(); ?>">
                                                                    <?php } else { ?>
                                                                        <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                                                             alt="<?php the_title(); ?>">
                                                                    <?php } ?>
                                                                </figure>
                                                                <div class="content">
                                                                    <p><span class="tdu"> <?php echo str_replace( 'Private: ', '', get_the_title()); ?></span></p>
	                                                                <?php if ( get_field('position') ) : ?>
                                                                        <em><?php echo esc_html(get_field('position')); ?></em>
	                                                                <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        <? } ?>
                                                    </div>
                                                <?php endwhile; ?>
                                            </div>
                                            <div class="sw_pagination"></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </section>
<?php } ?>


<?php get_footer(); ?>
