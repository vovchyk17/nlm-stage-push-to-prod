<?php get_header(); /* Template Name: Support */ ?>

<section class="support__top_panel bg"
         <?php echo has_post_thumbnail() ? 'style="'. esc_attr(image_src(get_post_thumbnail_id($post->ID), 'full', true)) .'"' : ''; ?>>
    <div class="container">
        <div class="content last_no_spacing">
            <?php if (have_posts()) : while (have_posts()) : the_post();
                the_content(); endwhile; endif; ?>
        </div>
        <?php if ($info_boxes = get_field('info_boxes' )) { ?>
            <div class="support__info_boxes">
                <?php foreach ($info_boxes as $box) { ?>
                    <div class="support__info_box">
                        <?php echo $box['title'] ? '<h4>'.esc_html($box['title']).'</h4>' : ''; ?>
                        <?php if( $link = $box['button'] ):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                        <?php endif; ?>
                        <?php echo wp_kses_post($box['text']); ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</section>
<?php if ($accordion = get_field('accordion' )) { ?>
    <section class="support__accordion">
        <div class="container" aria-label="Accordion">
            <?php echo get_field('title_collaborate') ? '<h2>'.esc_html(get_field('title_collaborate')).'</h2>' : ''; ?>
            <?php foreach ( $accordion as $row ) : ?>
                <div class="support__accordion_row">
                    <div class="acc_title h2" tabindex="0" aria-expanded="false"
                         aria-label="<?php echo esc_html( $row['title'] ); ?>">
                        <?php echo esc_html( $row['title'] ); ?>
                        <span class="circle_arrow is_down"></span>
                    </div>
                    <div class="content" aria-hidden="true">
                        <?php echo wp_kses_post( $row['text'] ); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php } ?>

<section class="support__bottom">
    <div class="container content">
        <div class="support__bottom__top_content content">
            <?php echo wp_kses_post(get_field('title_bottom')); ?>
        </div>
        <?php if ($image_bottom = get_field('image_bottom')) { ?>
            <figure class="support__bottom__image">
                <img src="<?php echo esc_url($image_bottom['url']); ?>" alt="<?php echo esc_attr($image_bottom['alt']); ?>">
            </figure>
        <?php } ?>
        <div class="support__bottom_content content">
            <?php echo wp_kses_post(get_field('text_bottom')); ?>
        </div>
    </div>
</section>


<?php get_footer(); ?>
