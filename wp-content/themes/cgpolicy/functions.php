<?php

// Run pre-installed plugins
require_once('inc/themer.php');

// uncomment if need CPT
require_once('inc/cpt.php');

//register menus
register_nav_menus(array(
    'main_menu' => 'Main menu',
    'footer_menu' => 'Footer menu',
    'footer_menu_2' => 'Footer menu 2',
));

//Custom images sizes
add_image_size( 'people_big', '260', '260', true );
add_image_size( 'people_medium', '190', '190', true );
add_image_size( 'people_small', '80', '80', true );
add_image_size( 'people_tiny', '50', '50', true );
add_image_size( 'featured_first_post', '688', '345', true );
add_image_size( 'featured_post', '330', '255', true );
add_image_size( 'post_item', '255', '151', true );
add_image_size( 'event_item', '330', '238', true );
add_image_size( 'fp_post_item', '350', '206', true );
add_image_size( 'home_main_post', '690', '320', true );
add_image_size( 'article_big', '540', '400', true );
add_image_size( 'article_middle', '351', '263', true );
add_image_size( 'article_small', '255', '190', true );
add_image_size( 'article_tiny', '90', '90', true );
add_image_size( 'article_tiny2', '100', '73', true );
add_image_size( 'article_popular', '324', '164', true );
add_image_size( 'photo_essay', '330', '341', true );
add_image_size( 'home_left_col', '290', '225', true );
add_image_size( 'home_right_col', '131', '86', true );
add_image_size( 'home_podcast', '168', '95', true );

//register sidebar
$reg_sidebars = array (
	'page_sidebar'     => 'Page Sidebar',
	'blog_sidebar'     => 'Blog Sidebar'
);
foreach ( $reg_sidebars as $id => $name ) {
	register_sidebar(
		array (
			'name'          => __( $name ),
			'id'            => $id,
			'before_widget' => '<div class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<mark class="widget_title">',
			'after_title'   => '</mark>',
		)
	);
}

if(function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title'    => 'Theme General Settings',
		'menu_title'    => 'Theme Settings',
		'menu_slug'     => 'theme-general-settings',
		'capability'    => 'edit_posts',
		'redirect'      => false
	));
}

function get_alt($id){
	$c_alt = get_post_meta($id, '_wp_attachment_image_alt', true);
	$c_tit = get_the_title($id);
	return $c_alt?$c_alt:$c_tit;
}

function cats($pid){
	$post_categories = wp_get_post_categories($pid);
	$cats = '';
	$co = count($post_categories); $i = 1;
	foreach($post_categories as $c){
		$cat = get_category($c);
		$abbr = get_field('abbreviation', $cat->taxonomy . '_' . $cat->term_id);
		$title = $abbr ? $abbr : $cat->name;
		$cats .= '<a href="'. get_category_link($cat->term_id) .'" class="cat_term">'. $title .'</a>' .($i++ != $co?'<span>,</span> ':'');
	}
	return $cats;
}

function full_cats($pid){
    $post_categories = wp_get_post_categories($pid);
    $cats = '';
    $co = count($post_categories); $i = 1;
    foreach($post_categories as $c){
        $cat = get_category($c);
        $title = $cat->name;
        $cats .= '<a href="'. get_category_link($cat->term_id) .'" class="cat_term">'. $title .'</a>' .($i++ != $co?'<span>,</span> ':'');
    }
    return $cats;
}

function get_current_url() {
	$pageURL = 'http';
	if (array_key_exists('HTTPS', $_SERVER) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return str_replace('www.', '', $pageURL);
}

function get_loader(){
	return '<div class="show_box"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"/></svg></div></div>';
}
// allowed tags to use loader with escaping
// usage - echo wp_kses(get_loader(), $GLOBALS['allowed_loader'])
$allowed_loader = array(
	'div'    => array(
		'class' => true
	),
	'svg'    => array(
		'class'   => true,
		'viewbox' => true,
	),
	'circle' => array(
		'class'             => true,
		'cx'                => true,
		'cy'                => true,
		'r'                 => true,
		'fill'              => true,
		'stroke-miterlimit' => true,
	),
);


function custom_tax($pid, $tax)
{
	$post_tax = get_the_terms($pid, $tax);
	$taxs = '';
	$co = count($post_tax);
	$i = 1;
	foreach ($post_tax as $t) {
		$tax = get_term($t);
		$abbr = get_field('abbreviation', $tax->taxonomy . '_' . $tax->term_id);
		$title = $abbr ? $abbr : $tax->name;
		$class = $abbr ? 'cat_term' : 'tax_term';
//		$taxs .= '<span class="tax_term">' . $tax->name . '</span>' . ($i++ != $co ? '<span>,</span> ' : '');
		$taxs .= '<a href="'. get_term_link($tax->term_id) .'" class="'.$class.'">'. $title .'</a>' . ($i++ != $co ? '<span>,</span> ' : '');
	}
	return $taxs;
}

/*function setPostViews($postID) {
	$countKey = 'post_views_count';
	$count = get_post_meta($postID, $countKey, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $countKey);
		add_post_meta($postID, $countKey, '0');
	}else{
		$count++;
		update_post_meta($postID, $countKey, $count);
	}
}*/

/**
 * Display the title and the publish date
 *
 * @param  string $post_html
 * @param  object $popular_post
 * @param  array  $instance
 * @return string
 */
function my_custom_single_popular_post($post_html, $popular_post, $instance){
	$output = '' ?>
	<div class="popular_post">
		<div class="popular_post__inner">
			<a href="<?php echo get_permalink($popular_post->id); ?>" class="thumb">
				<?php if ( has_post_thumbnail($popular_post->id) ) { ?>
					<img src="<?php echo esc_url(image_src(get_post_thumbnail_id($popular_post->id), 'article_popular')); ?>" alt="<?php echo esc_attr(get_alt($popular_post->id)) ?>">
				<?php } else { ?>
					<img src="<?php echo esc_url( theme() . '/images/placeholder-dark.png' ); ?>" alt="<?php $popular_post->title ?>">
				<?php } ?>
			</a>
			<div>
				<h4><a href="<?php echo get_permalink($popular_post->id); ?>"><?php echo get_field( 'alt_title', $popular_post->id ) ? esc_html( get_field( 'alt_title', $popular_post->id ) ) : $popular_post->title; ?></a></h4>
				<?php if ( $authors = get_field( 'authors', $popular_post->id ) ) : ?>
					<div class="post__authors">
						<?php
						$a_c = count( $authors );
						$a_i = 1;
						foreach ( $authors as $key => $value ) :
							$status = get_post_status( $value ); ?>
							<div class="post__author">
								<?php if ( $status === 'publish' ) { ?>
									<a href="<?php echo esc_url( get_permalink( $value ) ); ?>">
										<?php
										echo esc_html( get_the_title( $value ) );
										echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
										?>
									</a>
								<?php } elseif ( $status === 'private' ) { ?>
									<span>
                                                <?php
                                                echo str_replace( 'Private: ', '', get_the_title( $value ) );
                                                echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                                ?>
                                            </span>
								<? } ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<?php
	return $output;
}
add_filter( 'wpp_post', 'my_custom_single_popular_post', 10, 3 );

// display featured post thumbnails in WordPress feeds
function wcs_post_thumbnails_in_feeds( $content ) {
    global $post;
    if( has_post_thumbnail( $post->ID ) ) {
        $content = '<figure class="type:primaryImage">' . get_the_post_thumbnail( $post->ID ) . '</figure>' . get_the_excerpt($post->ID);
    }
    return $content;
}
add_filter( 'the_excerpt_rss', 'wcs_post_thumbnails_in_feeds' );

function wcs_post_thumbnails_in_content_feeds( $content ) {
    global $post;
    if( has_post_thumbnail( $post->ID ) ) {
        $content = '<figure class="type:primaryImage">' . get_the_post_thumbnail( $post->ID ) . '</figure>' . get_the_content($post->ID);
    }
    return $content;
}
add_filter( 'the_content_feed', 'wcs_post_thumbnails_in_content_feeds' );

/*function wcs_post_author_in_feeds($dc) {
    global $post;
    if ($authors = get_field('authors', $post->ID)) {
        foreach ($authors as $key => $author) :
            $dc = get_the_title($author);
        endforeach;
    } else {
        $dc = the_author();
    }
    return $dc;
}
add_filter('the_author', 'wcs_post_author_in_feeds');*/


add_action('init', 'customRSS');
function customRSS(){
    add_feed('newfeed', 'customRSSFunc');
}

function customRSSFunc(){
    get_template_part('rss', 'newfeed');
}


// add Author name to postmeta based on author (cpt writers) id assigned to the post - to appear in Search as meta value
add_filter('acf/update_value/name=authors', 'my_add_titles_to_post_for_search', 10, 3);
function my_add_titles_to_post_for_search($value, $post_id, $field) {
  delete_post_meta($post_id, '_hidden_relationship_author_title');
  if (!empty($value)) {
	  $posts = $value;
	  if (!is_array($posts)) {
		  $posts = array($posts);
	  }
	  foreach ($posts as $post) {
		  add_post_meta($post_id, '_hidden_relationship_author_title', get_the_title($post), false);
	  }
  }
  return $value;
}

// disable Yoast SEO @Person schema on posts
add_filter( 'wpseo_schema_needs_author', '__return_false' );

// change Yoast SEO article schema author to the custom value
add_filter( 'wpseo_schema_article', 'update_author_name__schema' );

function update_author_name__schema( $data ) {
    global $post;
    $user_name_arr = get_post_meta( $post->ID, '_hidden_relationship_author_title' );
    $user_name_full = $user_name_arr[0];
    $data['author'] = $user_name_full;
    return $data;
}

// change Yoast SEO article twitter/slack author to the custom value
add_filter( 'wpseo_enhanced_slack_data', 'update_author_name__twitter' );
function update_author_name__twitter( $tag_arr ) {
	global $post;
	$user_name_arr = get_post_meta( $post->ID, '_hidden_relationship_author_title' );
	$user_name_full = $user_name_arr[0];
	if( isset( $tag_arr['Written by'] ) ) {
		$tag_arr['Written by'] = $user_name_full;
	}
	return $tag_arr;
}
