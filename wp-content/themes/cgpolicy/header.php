<?php if (@ !WP_DEBUG) {
    ob_start('ob_html_compress');
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php wpa_title(); ?></title>
    <meta name="MobileOptimized" content="width"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#1b1725">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preload" as="style"
          href="https://fonts.googleapis.com/css2?family=Epilogue:wght@300;400;500;600;700&display=swap"/>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Epilogue:wght@300;400;500;600;700&display=swap" media="print"
          onload="this.media='all'"/>
    <?php wp_head(); ?>
    <!-- Google Analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-179038461-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
</head>
<?php if (is_singular('post')) {
    if (get_field('photo_essay') == true) {
        $single_pe_class = 'single-photo_essay';
    }
} ?>
<body <?php body_class($single_pe_class); ?> data-a="<?php echo esc_url(admin_url('admin-ajax.php')); ?>">
<div id="main">
    <?php
    $latest = get_field('donate', 'option');
    $subscribe = get_field('subscribe', 'option');
    ?>
    <header>
        <div class="header__top">
            <div class="container is_large flex">
                <div class="header__search_latest">
                    <a class="nav_icon" href="javascript:;"><i></i><i></i><i></i></a>
                    <div class="search_toggle rwd_hide">
                        <figure class="i_search-cgp"></figure>
                        <figure class="i_close"></figure>
                    </div>
                    <?php if ($latest['link']) : ?>
                        <a href="<?php echo esc_url($latest['link']); ?>"
                           class="button is_dark rwd_hide"><?php echo esc_html($latest['title']); ?></a>
                    <?php endif; ?>
                </div>

                <a href="<?php echo esc_url(site_url()); ?>" class="logo">
                    <?php if ( is_singular( 'post' ) ) {  ?>
						<?php if ( get_field( 'photo_essay' ) == true ) {  ?>
                            <img src="<?php echo esc_url( theme() . '/images/NL-Logo-White.svg' );  ?>" alt="Logo" class="light">
                            <img src="<?php echo esc_url( theme() . '/images/NL-Logo-Black.svg' );  ?>" alt="Logo" class="dark">
                        <?php } else {  ?>
                            <img src="<?php echo esc_url( theme() . '/images/NL-Logo-Black.svg' );  ?>" alt="Logo">
						<?php }  ?>
					<?php } else {  ?>
                        <img src="<?php echo esc_url( theme() . '/images/NL-Logo-Black.svg' );  ?>" alt="Logo">
					<?php }  ?>
                    <!--<img src="<?php /*echo esc_url(theme() . '/images/NL-Logo-Black.svg'); */?>" alt="Logo">-->
                </a>

                <div class="header__subscribe_button">
                    <?php if ($subscribe['link']) : ?>
                        <a href="<?php echo esc_url($subscribe['link']); ?>"
                           class="button mob_hide"><?php echo esc_html($subscribe['title']); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="header__bottom">
            <div class="container is_large">
                <?php if ( is_singular('post') && (get_field('photo_essay') == true) ) { ?>
                    <div class="flex">
                        <strong class="rwd_hide"><?php the_title(); ?></strong>
                        <div class="single_photo_essay__shrs">
                            <div class="single_photo_essay__shrs_inner">
                                <a class="i_fcbk_official"
                                   href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&quote=<?php echo get_field('share_text') ? get_field('share_text') : get_the_title(); ?>"
                                   title="Share at Facebook" target="_blank" rel="noopener"></a>
                                <a class="i_lnkdn"
                                   href="https://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"
                                   title="Share at LinkedIn" target="_blank" rel="noopener noreferrer"></a>
                                <a class="i_twtr"
                                   href="https://twitter.com/intent/tweet?text=<?php echo get_field('share_text') ? urlencode(get_field('share_text')) : urlencode(get_the_title()); ?> <?php the_permalink(); ?>"
                                   title="Tweet It" target="_blank" rel="noopener noreferrer"></a>
                                <a class="i_envelope_o"
                                   href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_title(); ?>&nbsp;-&nbsp;<?php the_permalink(); ?>"></a>
                                <a class="i_whtsp"
                                   href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>"
                                   data-action="share/whatsapp/share" target="_blank" rel="noopener noreferrer"></a>
                                <a class="i_print"  href="javaScript:window.print();" title="Print it"></a>
                            </div>
                        </div>
                    </div>
	            <?php } ?>

                <nav class="header__menu">
                    <div class="rwd_show"><?php get_search_form(); ?></div>
                    <?php if ($latest['link']) : ?>
                        <a href="<?php echo esc_url($latest['link']); ?>"
                           class="button is_dark rwd_show"><?php echo esc_html($latest['title']); ?></a>
                    <?php endif; ?>
                    <?php wp_nav_menu(array(
                        'container' => false,
                        'items_wrap' => '<ul class="flex_center__rwd">%3$s</ul>',
                        'theme_location' => 'main_menu'
                    )); ?>
                    <?php if ($subscribe['link']) : ?>
                        <a href="<?php echo esc_url($subscribe['link']); ?>"
                           class="button mob_show"><?php echo esc_html($subscribe['title']); ?></a>
                    <?php endif; ?>
                </nav>
            </div>
        </div>

        <div class="rwd_hide">
            <?php get_search_form(); ?>
        </div>
    </header>

    <div class="logo_print">
        <img src="<?php echo esc_url( theme() . '/images/NL-Logo-Black.svg' );  ?>" alt="Logo">
    </div>