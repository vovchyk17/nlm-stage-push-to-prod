</div>
<footer>
    <div class="container is_large flex_start__mob">
        <a href="<?php echo esc_url(site_url()) ?>" class="footer_logo">
            <img src="<?php echo esc_url(theme() . '/images/NL-Logo-White-No-Strap.svg') ?>" alt="white logo"/>
        </a>
        <nav class="footer_menu"><?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'footer_menu')); ?></nav>
        <div class="footer_contact">
            <?php echo wp_kses_post(so_me()); ?>
            <nav class="footer_menu_2"><?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'footer_menu_2')); ?></nav>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
<?php if (@!WP_DEBUG) {
    ob_end_flush();
} ?>
