<?php
get_header();

$top_args = array(
	'posts_per_page' => 2,
	'post_type'      => 'photo_essay',
	'post_status'    => 'publish',
);
?>

	<section class="container">
		<div class="articles__title">
			<h1>Photo Essays</h1>
		</div>

		<div class="articles__wrap">
			<div class="articles__top flex_start__rwd">
				<?php
				$top_posts = new WP_Query( $top_args );
				if($top_posts->have_posts()) : while ( $top_posts->have_posts() ) : $top_posts->the_post(); ?>
					<div class="articles__top_item">
						<a href="<?php the_permalink(); ?>">
							<?php if (has_post_thumbnail()) : ?>
								<picture>
									<source media="(min-width: 640px) and (max-width: 1024px)" srcset="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID), 'full')); ?>">
									<img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID), 'article_big')); ?>" alt="<?php the_title(); ?>">
								</picture>
							<?php else : ?>
								<img src="<?php echo esc_url(theme() . '/images/placeholder-dark.png'); ?>" alt="<?php the_title(); ?>">
							<?php endif; ?>
						</a>
						<div class="single_post__meta">
							<?php get_template_part( 'tpl-parts/cat-tax-group'); ?>
                            <time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date('F j. Y'); ?></time>
						</div>
						<h2><a href="<?php the_permalink(); ?>"><?php echo get_field('alt_title') ? esc_html(get_field('alt_title')) : the_title(); ?></a></h2>
						<p><?php echo has_excerpt() ? wp_kses_post( get_the_excerpt() ) : wp_kses_post( wp_trim_words( get_the_content(), 30 ) ); ?></p>
						<?php if ( $authors = get_field( 'authors' ) ) : ?>
							<div class="articles__top_authors">
								<?php
								$a_c = count($authors); $a_i = 1;
								foreach ( $authors as $key => $value ) :
									$status = get_post_status( $value );?>
									<div class="articles__top_author">
										<?php if ($status === 'publish') : ?>
											<a href="<?php echo esc_url(get_permalink($value)); ?>">
												<?php
												echo esc_html(get_the_title($value));
												echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
												?>
											</a>
										<?php elseif($status === 'private') : ?>
											<span>
                                            <?php
                                            echo str_replace( 'Private: ', '', get_the_title($value));
                                            echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
                                            ?>
                                        </span>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; endif; ?>
			</div>

			<div class="photo_essays__container">
				<?php photo_essays_ajax(); ?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>