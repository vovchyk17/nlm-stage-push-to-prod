<?php get_header(); /*Template Name: Contact*/ ?>
<section class="default_content container left_spacing">
    <div class="default_content__inner">
        <div class="default_content__title">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="content">
            <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
