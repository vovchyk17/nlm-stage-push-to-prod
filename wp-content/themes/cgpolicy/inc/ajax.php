<?php

// ajax posts load
function load_posts_ajax($fisrt_load = array()) {
	if ($fisrt_load) :
		extract($fisrt_load);
	else :
		extract($_POST);
	endif;

	global $post;

	$tax_query = array();

	if( $term != "*" && $term != null ) :
		$tax_query = array_merge($tax_query,array(
			'relation' => 'AND',
			array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => array($term)
			),
		));
	endif;

	$ppp = wp_is_mobile()?6:5;
	$offset_start = 2;
	$offset = ( $paged - 1 ) * $ppp + $offset_start;

	$args = array(
		'posts_per_page' => $ppp,
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'paged'          => $paged,
		'tax_query'      => $tax_query,
		'offset'         => $offset
	);

	$posts_query = new WP_Query( $args );

	$num_pages = $posts_query->max_num_pages;

	if($posts_query->have_posts()) : while ( $posts_query->have_posts() ) : $posts_query->the_post();
		get_template_part('tpl-parts/article-item');
	endwhile;

		if ($num_pages != $paged) :
			echo '<div class="load_more_holder">
                      <a class="button is_bigger load_more_posts" 
                         data-href="'. wp_kses_post(($paged + 1)) .'" 
                         data-tax="'. esc_html($taxonomy) .'"
                         data-term="'. esc_html($term) .'">Load more stories</a>
                  </div>
                  <div class="loader_holder">'. wp_kses(get_loader(), $GLOBALS['allowed_loader']) .'</div>';
		endif;

	else :
		echo '<div><h5 class="custom_coming_soon" hidden>Oops! Nothing found.</h5></div>';
	endif;

	if (wp_doing_ajax()) :
		wp_die();
	endif;

}
add_action( 'wp_ajax_load_posts_ajax', 'load_posts_ajax');
add_action( 'wp_ajax_nopriv_load_posts_ajax', 'load_posts_ajax');


// ajax photo_essays load
function photo_essays_ajax($paged = 1) {
	extract($_POST);

	global $post;

	$ppp = wp_is_mobile()?6:5;
	$offset_start = 2;
	$offset = ( $paged - 1 ) * $ppp + $offset_start;

	$args = array(
		'posts_per_page' => $ppp,
		'post_type'      => 'photo_essay',
		'post_status'    => 'publish',
		'paged'          => $paged,
		'offset'         => $offset
	);

	$posts_query = new WP_Query( $args );

	$num_pages = $posts_query->max_num_pages;

	if($posts_query->have_posts()) : while ( $posts_query->have_posts() ) : $posts_query->the_post();
		get_template_part('tpl-parts/photo_essay-item');
	endwhile;

		if ($num_pages != $paged) :
			echo '<div class="load_more_holder">
                      <a class="button is_bigger load_more_photo_essays" 
                         data-href="'. wp_kses_post(($paged + 1)) .'">Load more photo essays</a>
                  </div>
                  <div class="loader_holder">'. wp_kses(get_loader(), $GLOBALS['allowed_loader']) .'</div>';
		endif;

	else :
		echo '<div><h5 class="custom_coming_soon" hidden>Oops! Nothing found.</h5></div>';
	endif;

	if (wp_doing_ajax()) :
		wp_die();
	endif;

}
add_action( 'wp_ajax_photo_essays_ajax', 'photo_essays_ajax');
add_action( 'wp_ajax_nopriv_photo_essays_ajax', 'photo_essays_ajax');


// ajax search results
function load_search_ajax($fisrt_load = array(), $post_types = null) {
	add_filter( 'posts_search', 'advanced_custom_search', 500, 2 );

	if ($fisrt_load) :
		extract($fisrt_load);
	else :
		extract($_POST);
	endif;

	$jsonResult = array();

	global $post;

	$post_type = 'any';
	if($post_types)
	{
		$post_type = $post_types;
	}

	// the list of all post types
	$post_types_count = array(
		'post'        => 0,
		'photo_essay' => 0,
		'page'        => 0,
		'people'      => 0,
		'careers'     => 0,
	);


	// query to count amount of posts for each post type
	$s_counting = array(
		'posts_per_page' => - 1,
		'post_type'      => $post_type,
		'post_status'    => 'publish',
		'post__not_in'   => array( 2 ),
		's'              => $s,
	);

	$s_counting_query = new WP_Query( $s_counting );

	foreach($s_counting_query->posts as $item)
	{
		$post_type_name = get_post_type($item->ID);
		$post_types_count[$post_type_name] = $post_types_count[$post_type_name]+1;
	}

	// query to output needed amount
	$s_args = array(
		'posts_per_page'      => 4,
		'paged'               => $paged,
		'post_type'           => $post_type,
		'post_status'         => 'publish',
		'post__not_in'        => array( 2 ),
		's'                   => $s,
		'ignore_sticky_posts' => 1
	);

	$s_query = new WP_Query( $s_args );

	$num_pages = $s_query->max_num_pages;
	ob_start();
	if($s_query->have_posts()) : while ( $s_query->have_posts() ) : $s_query->the_post();

		$pt = get_post_type($post->ID); ?>

		<div class="search_post content is_pt_<?php echo esc_html($pt); ?>">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<p><?php echo wp_kses_post(wp_trim_words( get_the_content(), 35 )); ?></p>
			<a class="button" href="<?php the_permalink(); ?>">Read more</a>
		</div>

	<?php endwhile;

		if ($num_pages != $paged) :
			echo '<div class="load_more_holder">
                      <a class="button is_bigger load_more__search" 
                      		data-href="'. wp_kses_post(($paged + 1)) .'" 
                      		data-search="'. wp_kses_post($s) .'">Load more results</a>
                  </div>
                  <div class="loader_holder">'. wp_kses(get_loader(), $GLOBALS['allowed_loader']) .'</div>';
		endif;

	else :
		echo '<div><em class="custom_coming_soon">Oops! Nothing found.</em></div>';
	endif;
	$str = ob_get_clean();

	$jsonResult['html']  = $str;
	$jsonResult['foundPosts']  = $s_query->found_posts;
	$jsonResult['post_types_count'] = $post_types_count;


	
	remove_filter( 'posts_search', 'advanced_custom_search', 500 );

	if ( wp_doing_ajax() ) {
		die( json_encode( $jsonResult ) );
	}
	return $jsonResult;

}
add_action( 'wp_ajax_load_search_ajax', 'load_search_ajax');
add_action( 'wp_ajax_nopriv_load_search_ajax', 'load_search_ajax');


function advanced_custom_search( $where, &$wp_query ) {
	global $wpdb;

	if ( empty( $where ))
		return $where;

	$terms = $wp_query->query_vars[ 's' ];

	$exploded = explode( ' ', $terms );
    if( $exploded === FALSE || count( $exploded ) == 0 )
        $exploded = array( 0 => $terms );

	$where = '';

	$list_searcheable_acf = array("title", "sub_title", "excerpt_short", "excerpt_long", "alt_title");




	foreach( $exploded as $tag ) :

		$tag = preg_replace(array('/\b"/', '/"/', "/`/", "/“/", "/”/", "/‘/"), array('','','','','',''), $tag);

		$apostrophe = (preg_grep('/(^[\w]+[’|\']+[\w])/mi', explode("\n", $tag)));
		if(!$apostrophe){
			$tag = str_replace("'", '', $tag);
			$tag = str_replace("’", '', $tag);
		}
		else{
			$tag = str_replace('\'', '’', $tag);
		}

	$where .= " 
	AND (
		(wp_posts.post_title LIKE \"%$tag%\")
		OR (wp_posts.post_content LIKE \"%$tag%\")
		OR EXISTS (
		SELECT * FROM wp_postmeta
			WHERE post_id = wp_posts.ID
				AND (";
	foreach ($list_searcheable_acf as $searcheable_acf) :
	if ($searcheable_acf == $list_searcheable_acf[0]):
		$where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE \"%$tag%\") ";
	else :
		$where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE \"%$tag%\") ";
	endif;
	endforeach;
		$where .= ")
		)
		OR EXISTS (
		SELECT * FROM wp_comments
		WHERE comment_post_ID = wp_posts.ID
			AND comment_content LIKE \"%$tag%\"
		)
		OR EXISTS (
		SELECT * FROM wp_terms
		INNER JOIN wp_term_taxonomy
			ON wp_term_taxonomy.term_id = wp_terms.term_id
		INNER JOIN wp_term_relationships
			ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
		WHERE (
			taxonomy = 'post_tag'
			)
			AND object_id = wp_posts.ID
			AND wp_terms.name LIKE \"%$tag%\"
		)
	)";
		endforeach;
	return $where;
}



/*add_action('init', function () {
    if(isset($_GET['test123'])) {
	    $posts = new WP_Query([
		    'posts_per_page' => - 1,
		    'post_type'      => 'post',
		    'post_status'    => 'publish',
	    ]);

        $i = 1;
	    foreach($posts->posts as $item):
		    var_dump( update_field('authors', get_field('authors', $item->ID), $item->ID), $item->ID .'+'. $i++ );
	    endforeach;

        die();
    }
});*/