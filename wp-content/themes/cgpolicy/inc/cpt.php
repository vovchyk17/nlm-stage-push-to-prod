<?php
//Example for Custom Post Type with Taxonomies

/*
    *** You van use dash-icons https://developer.wordpress.org/resource/dashicons/
*/
add_action('init', 'register_cpts');
function register_cpts()
{

	// custom taxonomy Programs attached to 'post'
	/*$taxname = 'Program';
	$taxlabels = array(
		'name'                          => $taxname,
		'singular_name'                 => $taxname,
		'search_items'                  => 'Search '.$taxname,
		'popular_items'                 => 'Popular '.$taxname,
		'all_items'                     => 'All '.$taxname.'s',
		'parent_item'                   => 'Parent '.$taxname,
		'edit_item'                     => 'Edit '.$taxname,
		'update_item'                   => 'Update '.$taxname,
		'add_new_item'                  => 'Add New '.$taxname,
		'new_item_name'                 => 'New '.$taxname,
		'separate_items_with_commas'    => 'Separate '.$taxname.'s with commas',
		'add_or_remove_items'           => 'Add or remove '.$taxname.'s',
		'choose_from_most_used'         => 'Choose from most used '.$taxname.'s'
	);
	$taxarr = array(
		'label'                         => $taxname,
		'labels'                        => $taxlabels,
		'public'                        => true,
		'hierarchical'                  => true,
		'show_in_nav_menus'             => true,
		'args'                          => array( 'orderby' => 'term_order' ),
		'query_var'                     => true,
		'show_ui'                       => true,
		'rewrite'                       => true,
		'show_admin_column'             => true,
		'show_in_rest'                  => true
	);
	register_taxonomy( 'programs', 'post', $taxarr );*/


	// custom taxonomy Analysis attached to 'post'
	$taxname = 'Topic';
	$taxlabels = array(
		'name'                          => $taxname.'s',
		'singular_name'                 => $taxname,
		'search_items'                  => 'Search '.$taxname,
		'popular_items'                 => 'Popular '.$taxname,
		'all_items'                     => 'All Topics',
		'parent_item'                   => 'Parent '.$taxname,
		'edit_item'                     => 'Edit '.$taxname,
		'update_item'                   => 'Update '.$taxname,
		'add_new_item'                  => 'Add New '.$taxname,
		'new_item_name'                 => 'New '.$taxname,
		'separate_items_with_commas'    => 'Separate Topics with commas',
		'add_or_remove_items'           => 'Add or remove Topics',
		'choose_from_most_used'         => 'Choose from most used Topics'
	);
	$taxarr = array(
		'label'                         => $taxname,
		'labels'                        => $taxlabels,
		'public'                        => true,
		'hierarchical'                  => true,
		'show_in_nav_menus'             => true,
		'args'                          => array( 'orderby' => 'term_order' ),
		'query_var'                     => true,
		'show_ui'                       => true,
		'rewrite'                       => array('slug' => 'topic'),
		'show_admin_column'             => true,
		'show_in_rest'                  => true
	);
	register_taxonomy( 'analysis', 'post', $taxarr );


    //custom taxonomy attached to CPT People
    $taxname = 'Category';
    $taxlabels = array(
        'name'                          => 'Categories',
        'singular_name'                 => $taxname,
        'search_items'                  => 'Search '.$taxname,
        'popular_items'                 => 'Popular '.$taxname,
        'all_items'                     => 'All Categories',
        'parent_item'                   => 'Parent '.$taxname,
        'edit_item'                     => 'Edit '.$taxname,
        'update_item'                   => 'Update '.$taxname,
        'add_new_item'                  => 'Add New '.$taxname,
        'new_item_name'                 => 'New '.$taxname,
        'separate_items_with_commas'    => 'Separate Categories with commas',
        'add_or_remove_items'           => 'Add or remove Categories',
        'choose_from_most_used'         => 'Choose from most used Categories'
    );
    $taxarr = array(
        'label'                         => $taxname,
        'labels'                        => $taxlabels,
        'public'                        => true,
        'hierarchical'                  => true,
        'show_in_nav_menus'             => true,
        'args'                          => array( 'orderby' => 'term_order' ),
        'query_var'                     => true,
        'show_ui'                       => true,
        'show_in_rest'                  => true,
        'rewrite'                       => true,
        'show_admin_column'             => true
    );
    register_taxonomy('people_cat', 'people', $taxarr);

    register_post_type('people',
        array(
            'labels' => array(
                'name' => 'Writers',
                'singular_name' => 'Writer',
                'menu_name' => 'Writers'
            ),
            'public'            => true,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'supports'          => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'           => array( 'slug' => 'writers' ),
            'has_archive'       => false,
            'hierarchical'      => true,
            'show_in_nav_menus' => true,
            'show_in_rest'      => true,
            'capability_type'   => 'page',
            'query_var'         => true,
            'menu_icon'         => 'dashicons-groups',
        ));

    //custom taxonomy attached to CPT In the Media
    /*$taxname = 'Op-Eds';
    $taxlabels = array(
        'name'                          => $taxname,
        'singular_name'                 => $taxname,
        'search_items'                  => 'Search '.$taxname,
        'popular_items'                 => 'Popular '.$taxname,
        'all_items'                     => 'All Categories',
        'parent_item'                   => 'Parent '.$taxname,
        'edit_item'                     => 'Edit '.$taxname,
        'update_item'                   => 'Update '.$taxname,
        'add_new_item'                  => 'Add New '.$taxname,
        'new_item_name'                 => 'New '.$taxname,
        'separate_items_with_commas'    => 'Separate '.$taxname.' with commas',
        'add_or_remove_items'           => 'Add or remove '.$taxname,
        'choose_from_most_used'         => 'Choose from most used '.$taxname
    );
    $taxarr = array(
        'label'                         => $taxname,
        'labels'                        => $taxlabels,
        'public'                        => true,
        'hierarchical'                  => true,
        'show_in_nav_menus'             => true,
        'args'                          => array( 'orderby' => 'term_order' ),
        'query_var'                     => true,
        'show_ui'                       => true,
        'show_in_rest'                  => true,
        'rewrite'                       => true,
        'show_admin_column'             => true
    );
    register_taxonomy('op_eds_itm', 'in_the_media', $taxarr);

    $taxname = 'In the News';
    $taxlabels = array(
        'name'                          => $taxname,
        'singular_name'                 => $taxname,
        'search_items'                  => 'Search '.$taxname,
        'popular_items'                 => 'Popular '.$taxname,
        'all_items'                     => 'All Categories',
        'parent_item'                   => 'Parent '.$taxname,
        'edit_item'                     => 'Edit '.$taxname,
        'update_item'                   => 'Update '.$taxname,
        'add_new_item'                  => 'Add New '.$taxname,
        'new_item_name'                 => 'New '.$taxname,
        'separate_items_with_commas'    => 'Separate '.$taxname.' with commas',
        'add_or_remove_items'           => 'Add or remove '.$taxname,
        'choose_from_most_used'         => 'Choose from most used '.$taxname
    );
    $taxarr = array(
        'label'                         => $taxname,
        'labels'                        => $taxlabels,
        'public'                        => true,
        'hierarchical'                  => true,
        'show_in_nav_menus'             => true,
        'args'                          => array( 'orderby' => 'term_order' ),
        'query_var'                     => true,
        'show_ui'                       => true,
        'show_in_rest'                  => true,
        'rewrite'                       => true,
        'show_admin_column'             => true
    );
    register_taxonomy('in_the_news_itm', 'in_the_media', $taxarr);

    $taxname = 'Press Releases';
    $taxlabels = array(
        'name'                          => $taxname,
        'singular_name'                 => $taxname,
        'search_items'                  => 'Search '.$taxname,
        'popular_items'                 => 'Popular '.$taxname,
        'all_items'                     => 'All Categories',
        'parent_item'                   => 'Parent '.$taxname,
        'edit_item'                     => 'Edit '.$taxname,
        'update_item'                   => 'Update '.$taxname,
        'add_new_item'                  => 'Add New '.$taxname,
        'new_item_name'                 => 'New '.$taxname,
        'separate_items_with_commas'    => 'Separate '.$taxname.' with commas',
        'add_or_remove_items'           => 'Add or remove '.$taxname,
        'choose_from_most_used'         => 'Choose from most used '.$taxname
    );
    $taxarr = array(
        'label'                         => $taxname,
        'labels'                        => $taxlabels,
        'public'                        => true,
        'hierarchical'                  => true,
        'show_in_nav_menus'             => true,
        'args'                          => array( 'orderby' => 'term_order' ),
        'query_var'                     => true,
        'show_ui'                       => true,
        'show_in_rest'                  => true,
        'rewrite'                       => true,
        'show_admin_column'             => true
    );
    register_taxonomy('press_releases_itm', 'in_the_media', $taxarr);

    register_post_type('in_the_media',
        array(
            'labels' => array(
                'name' => 'In the Media',
                'singular_name' => 'In the Media',
                'menu_name' => 'In the Media'
            ),
            'public'            => true,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'supports'          => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'           => array( 'slug' => 'in-the-media' ),
            'has_archive'       => false,
            'hierarchical'      => true,
            'show_in_nav_menus' => true,
            'show_in_rest'      => true,
            'capability_type'   => 'page',
            'query_var'         => true,
            'menu_icon'         => 'dashicons-megaphone',
        ));*/

    register_post_type('careers',
        array(
            'labels' => array(
                'name' => 'Сareers',
                'singular_name' => 'Сareers',
                'menu_name' => 'Сareers'
            ),
            'public'            => true,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'supports'          => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'           => array( 'slug' => 'careers' ),
            'has_archive'       => false,
            'hierarchical'      => true,
            'show_in_nav_menus' => true,
            'show_in_rest'      => true,
            'capability_type'   => 'page',
            'query_var'         => true,
            'menu_icon'         => 'dashicons-businessman',
        ));

	/*register_post_type('event',
		array(
			'labels' => array(
				'name' => 'Events',
				'singular_name' => 'Event',
				'menu_name' => 'Events'
			),
			'public'            => true,
			'show_ui'           => true,
			'show_in_menu'      => true,
			'supports'          => array( 'title', 'editor', 'thumbnail' ),
			'rewrite'           => array( 'slug' => 'events' ),
			'has_archive'       => false,
			'hierarchical'      => true,
			'show_in_nav_menus' => true,
			'show_in_rest'      => true,
			'capability_type'   => 'page',
			'query_var'         => true,
			'menu_icon'         => 'dashicons-tickets-alt',
			'menu_position'     => 5
		));*/

	//custom taxonomy attached to CPT Photo essay
	/*$taxname = 'Category';
	$taxlabels = array(
		'name'                          => 'Categories',
		'singular_name'                 => $taxname,
		'search_items'                  => 'Search '.$taxname,
		'popular_items'                 => 'Popular '.$taxname,
		'all_items'                     => 'All Categories',
		'parent_item'                   => 'Parent '.$taxname,
		'edit_item'                     => 'Edit '.$taxname,
		'update_item'                   => 'Update '.$taxname,
		'add_new_item'                  => 'Add New '.$taxname,
		'new_item_name'                 => 'New '.$taxname,
		'separate_items_with_commas'    => 'Separate Categories with commas',
		'add_or_remove_items'           => 'Add or remove Categories',
		'choose_from_most_used'         => 'Choose from most used Categories'
	);
	$taxarr = array(
		'label'                         => $taxname,
		'labels'                        => $taxlabels,
		'public'                        => true,
		'hierarchical'                  => true,
		'show_in_nav_menus'             => true,
		'args'                          => array( 'orderby' => 'term_order' ),
		'query_var'                     => true,
		'show_ui'                       => true,
		'show_in_rest'                  => true,
		'rewrite'                       => true,
		'show_admin_column'             => true
	);
	register_taxonomy('photo_essay_cat', 'photo_essay', $taxarr);

	register_post_type('photo_essay',
		array(
			'labels' => array(
				'name' => 'Photo essays',
				'singular_name' => 'Photo essay',
				'menu_name' => 'Photo essays'
			),
			'public'            => true,
			'show_ui'           => true,
			'show_in_menu'      => true,
			'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'rewrite'           => array( 'slug' => 'photo-essays-tax' ),
			'has_archive'       => true,
			'hierarchical'      => true,
			'show_in_nav_menus' => true,
			'show_in_rest'      => true,
			'capability_type'   => 'page',
			'query_var'         => true,
			'menu_icon'         => 'dashicons-camera-alt',
		));*/

    if (defined('WP_DEBUG') && true !== WP_DEBUG) {
        flush_rewrite_rules();
    }
}

