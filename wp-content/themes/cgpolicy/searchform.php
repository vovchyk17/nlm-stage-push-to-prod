<div class="search_form">
    <form class="flex" role="search" method="get" action="<?php echo esc_url(home_url( '/' )); ?>">
        <input type="text" value="<?php echo get_search_query(); ?>" placeholder="Search..." name="s" id="s"/>
        <button type="submit" class="i_search-cgp search_results_submit"></button>
        <?php echo wp_kses(get_loader(), $GLOBALS['allowed_loader']); ?>
    </form>
</div>