<?php get_header(); /*Template Name: Newsletter*/ ?>
    <section class="default_content container left_spacing">
        <div class="default_content__inner">
            <div class="default_content__title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="content">
                <?php echo do_shortcode('[contact-form-7 id="18690" title="Newsletter"]'); ?>
            </div>
        </div>
    </section>

<?php
echo get_field('checkbox_1') ? '<small class="nwsltr_checkbox_desc_1">' .get_field('checkbox_1'). '</small>' :'';
echo get_field('checkbox_2') ? '<small class="nwsltr_checkbox_desc_2">' .get_field('checkbox_2'). '</small>' :'';
echo get_field('checkbox_3') ? '<small class="nwsltr_checkbox_desc_3">' .get_field('checkbox_3'). '</small>' :'';
echo get_field('checkbox_4') ? '<small class="nwsltr_checkbox_desc_4">' .get_field('checkbox_4'). '</small>' :'';
echo get_field('checkbox_5') ? '<small class="nwsltr_checkbox_desc_5">' .get_field('checkbox_5'). '</small>' :'';
?>


<?php get_footer(); ?>