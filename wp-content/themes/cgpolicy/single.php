<?php get_header(); ?>


<?php if (get_field('photo_essay') == true) { ?>
    <?php if ($parallax_sections = get_field('parallax_sections')) { ?>
        <section class="single_photo_essay__parallax">
            <div class="single_photo_essay__parallax_wrapper">
                <?php $i = 1;
                foreach ($parallax_sections as $ps) { ?>
                    <figure id="p_img_<?php echo esc_attr($i); ?>"
                            class="single_photo_essay__parallax_image<?php echo ($i == 1) ? ' is_active' : ''; ?>">
                        <img src="<?php echo esc_url(image_src($ps['image']['ID'], 'full')); ?>"
                             alt="<?php echo esc_attr($ps['image']['alt']); ?>">
                    </figure>
                    <?php $i++;
                } ?>
            </div>
            <?php $k = 1;
            foreach ($parallax_sections as $ps) { ?>
                <div id="p_text_<?php echo esc_attr($k); ?>" class="single_photo_essay__parallax_text">
                    <div class="container">
                        <div class="content<?php echo ($ps['text_color'] === 'dark') ? ' is_darken' : ''; ?>">
                            <?php echo wp_kses_post($ps['text']) ?>
                        </div>
                    </div>
                </div>
                <?php $k++;
            } ?>
            <ul class="single_photo_essay__parallax_nav">
                <?php $l = 1;
                foreach ($parallax_sections as $ps) { ?>
                    <li><a href="#to-p_text_<?php echo esc_attr($l); ?>"></a></li>
                    <?php $l++;
                } ?>
            </ul>
        </section>
    <?php } ?>
    <section class="single_photo_essay__main_content">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="container">
                <div class="single_post__meta">
                    <?php get_template_part('tpl-parts/cat-tax-group'); ?>
	                <?php if (!in_category( 'podcast', $post->ID)) { ?>
		                <?php
		                $post_article = $post->post_content;
		                $words_amount = str_word_count(wp_strip_all_tags($post_article));
		                $words_per_minute = 220;
		                $minutes = floor($words_amount / $words_per_minute);
		                $seconds = floor($words_amount % $words_per_minute / ($words_per_minute / 60));
		                $min_to_read = ($seconds > 30) ? $minutes + 1 : $minutes;
		                $time_to_read = get_field('time_to_read') ? get_field('time_to_read') : $min_to_read;
		                ?>
                        <div class="min_to_read"><?php echo esc_html($time_to_read); ?> min read</div>
	                <?php } ?>
                </div>
                <h1><?php the_title(); ?></h1>
                <?php echo get_field('sub_title') ? '<h2>' . esc_html(get_field('sub_title')) . '</h2>' : ''; ?>
                <div class="single_post__author_date flex_start__mob">
                    <?php if ($authors = get_field('authors')) : ?>
                        <div class="single_post__authors">
                            <?php foreach ($authors as $key => $author) :
                                $status = get_post_status($author); ?>
                                <div class="single_post__author">
                                    <figure>
                                        <?php if ($status === 'publish') : ?><a
                                                href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                            <?php if (has_post_thumbnail($author)) { ?>
                                                <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($author), 'people_small')); ?>"
                                                     alt="<?php echo esc_attr(get_alt($author)); ?>">
                                            <?php } else { ?>
                                                <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                                     alt="<?php the_title(); ?>">
                                            <?php } ?>
                                            <?php if ($status === 'publish') : ?></a><?php endif; ?>
                                    </figure>
                                    <small>
                                        <strong>
                                            <?php if ($status === 'publish') : ?><a
                                                    href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                                <?php echo esc_html(get_the_title($author)); ?>
                                                <?php if ($status === 'publish') : ?></a><?php endif; ?>
                                        </strong>
                                        <?php if (get_field('desc', $author)) : ?>
                                            <em><?php echo esc_html(get_field('desc', $author)); ?></em>
                                        <?php endif; ?>
                                    </small>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <time datetime="<?php echo get_the_date('Y-m-d'); ?>">
                        <?php echo get_the_date('F j, Y'); ?>
                    </time>
                </div>
            </div>
            <div class="single_photo_essay__shrs_wrap container">
                <div class="content">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </section>
    <?php if ($additional_sections = get_field('additional_sections')) { ?>
        <section class="single_photo_essay__additional">
            <?php foreach ($additional_sections as $as) { ?>
                <div class="single_photo_essay__additional_section">
                    <figure class="single_photo_essay__additional_img<?php echo ($as['full_width_image'] == true) ? '' : ' container'; ?>">
                        <img src="<?php echo esc_url(image_src($as['image']['ID'], 'full')); ?>"
                             alt="<?php echo esc_attr($as['image']['alt']) ?>">
                    </figure>
                    <div class="single_photo_essay__additional_text container">
                        <div class="content">
                            <?php echo wp_kses_post($as['text']) ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </section>
    <?php } ?>
    <?php if ($authors = get_field('authors')) : ?>
        <section class="single_photo_essay__authors rwd_hide">
            <div class="container">
                <?php foreach ($authors as $key => $author) :
                    $status = get_post_status($author); ?>
                    <div class="single_post__author">
                        <figure>
                            <?php if ($status === 'publish') : ?><a
                                    href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                <?php if (has_post_thumbnail($author)) { ?>
                                    <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($author), 'thumbnail')); ?>"
                                         alt="<?php echo esc_attr(get_alt($author)); ?>">
                                <?php } else { ?>
                                    <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                         alt="<?php the_title(); ?>">
                                <?php } ?>
                                <?php if ($status === 'publish') : ?></a><?php endif; ?>
                        </figure>
                        <small>
                            <strong>
                                <?php if ($status === 'publish') : ?><a
                                        href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                    <?php echo esc_html(get_the_title($author)); ?>
                                    <?php if ($status === 'publish') : ?></a><?php endif; ?>
                            </strong>
                            <?php if (get_field('desc', $author)) : ?>
                                <em><?php echo esc_html(get_field('desc', $author)); ?></em>
                            <?php endif; ?>
                        </small>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
    <?php endif; ?>
<?php } elseif (get_field('special_top_section') == true) { ?>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="single_post">
            <div class="single_post__special_top container is_large">
                <div class="single_post__special_top_left">
                    <div class="single_post__meta">
	                    <?php get_template_part('tpl-parts/cat-tax-group'); ?>
                        <?php if (!in_category( 'podcast', $post->ID)) { ?>
                            <?php
                            $post_article = $post->post_content;
                            $words_amount = str_word_count(wp_strip_all_tags($post_article));
                            $words_per_minute = 220;
                            $minutes = floor($words_amount / $words_per_minute);
                            $seconds = floor($words_amount % $words_per_minute / ($words_per_minute / 60));
                            $min_to_read = ($seconds > 30) ? $minutes + 1 : $minutes;
                            $time_to_read = get_field('time_to_read') ? get_field('time_to_read') : $min_to_read;
                            ?>
                            <div class="min_to_read"><?php echo esc_html($time_to_read); ?> min read</div>
                        <?php } ?>
                    </div>
                    <h1><?php the_title(); ?></h1>
                    <?php echo get_field('sub_title') ? '<h2>' . esc_html(get_field('sub_title')) . '</h2>' : ''; ?>
                    <div class="single_post__author_date flex_start__mob">
                        <?php if ($authors = get_field('authors')) : ?>
                            <div class="single_post__authors">
                                <?php foreach ($authors as $key => $author) :
                                    $status = get_post_status($author); ?>
                                    <div class="single_post__author">
                                        <figure>
                                            <?php if ($status === 'publish') : ?><a
                                                    href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                                <?php if (has_post_thumbnail($author)) { ?>
                                                    <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($author), 'people_small')); ?>"
                                                         alt="<?php echo esc_attr(get_alt($author)); ?>">
                                                <?php } else { ?>
                                                    <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                                         alt="<?php the_title(); ?>">
                                                <?php } ?>
                                                <?php if ($status === 'publish') : ?></a><?php endif; ?>
                                        </figure>
                                        <small>
                                            <strong>
                                                <?php if ($status === 'publish') : ?><a
                                                        href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                                    <?php echo esc_html(get_the_title($author)); ?>
                                                    <?php if ($status === 'publish') : ?></a><?php endif; ?>
                                            </strong>
                                            <?php if (get_field('desc', $author)) : ?>
                                                <em><?php echo esc_html(get_field('desc', $author)); ?></em>
                                            <?php endif; ?>
                                        </small>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                        <time datetime="<?php echo get_the_date('Y-m-d'); ?>">
                            <?php echo get_the_date('F j, Y'); ?>
                        </time>
                    </div>
                </div>
                <figure class="single_post__thumb">
                    <?php if (has_post_thumbnail()) : ?>
                        <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID))); ?>"
                             alt="<?php the_title(); ?>">
                        <?php if (get_the_post_thumbnail_caption()) : ?>
                            <figcaption>
                                <?php the_post_thumbnail_caption(); ?>
                            </figcaption>
                        <?php endif; ?>
                    <?php else : ?>
                        <img src="<?php echo esc_url(theme() . '/images/placeholder-dark.png'); ?>"
                             alt="<?php the_title(); ?>">
                    <?php endif; ?>
                </figure>
            </div>
            <div class="single_post__body_wrap container">
                <div class="single_post__shrs">
                    <small class="mob_hide">Share</small>
                    <div class="single_post__shrs_inner">
                        <a class="i_fcbk_official"
                           href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&quote=<?php echo get_field('share_text') ? get_field('share_text') : get_the_title(); ?>"
                           title="Share at Facebook" target="_blank" rel="noopener"></a>
                        <a class="i_lnkdn"
                           href="https://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"
                           title="Share at LinkedIn" target="_blank" rel="noopener noreferrer"></a>
                        <a class="i_twtr"
                           href="https://twitter.com/intent/tweet?text=<?php echo get_field('share_text') ? urlencode(get_field('share_text')) : urlencode(get_the_title()); ?> <?php the_permalink(); ?>"
                           title="Tweet It" target="_blank" rel="noopener noreferrer"></a>
                        <a class="i_envelope_o"
                           href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_title(); ?>&nbsp;-&nbsp;<?php the_permalink(); ?>"></a>
                        <a class="i_whtsp" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>"
                           data-action="share/whatsapp/share" target="_blank" rel="noopener noreferrer"></a>
                        <a class="i_print"  href="javaScript:window.print();" title="Print it"></a>
                    </div>
                </div>
                <div class="single_post__content">
                    <div class="content">
                        <?php the_content(); ?>
                    </div>

                    <?php
                    $tag_taxs = get_the_terms($post->ID, 'post_tag');
                    if (!empty($tag_taxs) && !is_wp_error($tag_taxs)) : ?>
                        <div class="single_post__tags">
                            <strong>TAGS:</strong>
                            <?php echo wp_kses_post(custom_tax($post->ID, 'post_tag')); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>
<?php } else { ?>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="single_post">
            <div class="single_post__top container">
                <div class="single_post__meta">
                    <?php get_template_part('tpl-parts/cat-tax-group'); ?>
                    <?php if (!in_category( 'podcast', $post->ID)) { ?>
                        <?php
                        $post_article = $post->post_content;
                        $words_amount = str_word_count(wp_strip_all_tags($post_article));
                        $words_per_minute = 220;
                        $minutes = floor($words_amount / $words_per_minute);
                        $seconds = floor($words_amount % $words_per_minute / ($words_per_minute / 60));
                        $min_to_read = ($seconds > 30) ? $minutes + 1 : $minutes;
                        $time_to_read = get_field('time_to_read') ? get_field('time_to_read') : $min_to_read;
                        ?>
                        <div class="min_to_read"><?php echo esc_html($time_to_read); ?> min read</div>
                    <?php } ?>
                </div>
                <h1><?php the_title(); ?></h1>
                <?php echo get_field('sub_title') ? '<h2>' . esc_html(get_field('sub_title')) . '</h2>' : ''; ?>

                <div class="single_post__author_date flex_start__mob">
                    <?php if ($authors = get_field('authors')) : ?>
                        <div class="single_post__authors">
                            <?php foreach ($authors as $key => $author) :
                                $status = get_post_status($author); ?>
                                <div class="single_post__author">
                                    <figure>
                                        <?php if ($status === 'publish') : ?><a
                                                href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                            <?php if (has_post_thumbnail($author)) { ?>
                                                <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($author), 'people_small')); ?>"
                                                     alt="<?php echo esc_attr(get_alt($author)); ?>">
                                            <?php } else { ?>
                                                <img src="<?php echo esc_url(theme() . '/images/author-placeholder.png') ?>"
                                                     alt="<?php the_title(); ?>">
                                            <?php } ?>
                                            <?php if ($status === 'publish') : ?></a><?php endif; ?>
                                    </figure>
                                    <small>
                                        <strong>
                                            <?php if ($status === 'publish') : ?><a
                                                    href="<?php echo esc_url(get_permalink($author)); ?>"><?php endif; ?>
                                                <?php echo esc_html(get_the_title($author)); ?>
                                                <?php if ($status === 'publish') : ?></a><?php endif; ?>
                                        </strong>
                                        <?php if (get_field('desc', $author)) : ?>
                                            <em><?php echo esc_html(get_field('desc', $author)); ?></em>
                                        <?php endif; ?>
                                    </small>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <time datetime="<?php echo get_the_date('Y-m-d'); ?>">
                        <?php echo get_the_date('F j, Y'); ?>
                    </time>
                </div>
            </div>
            <div class="single_post__body_wrap container">
                <div class="single_post__shrs">
                    <small class="mob_hide">Share</small>
                    <div class="single_post__shrs_inner">
                        <a class="i_fcbk_official"
                           href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&quote=<?php echo get_field('share_text') ? get_field('share_text') : get_the_title(); ?>"
                           title="Share at Facebook" target="_blank" rel="noopener"></a>
                        <a class="i_lnkdn"
                           href="https://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>"
                           title="Share at LinkedIn" target="_blank" rel="noopener noreferrer"></a>
                        <a class="i_twtr"
                           href="https://twitter.com/intent/tweet?text=<?php echo get_field('share_text') ? urlencode(get_field('share_text')) : urlencode(get_the_title()); ?> <?php the_permalink(); ?>"
                           title="Tweet It" target="_blank" rel="noopener noreferrer"></a>
                        <a class="i_envelope_o"
                           href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_title(); ?>&nbsp;-&nbsp;<?php the_permalink(); ?>"></a>
                        <a class="i_whtsp" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>"
                           data-action="share/whatsapp/share" target="_blank" rel="noopener noreferrer"></a>
                        <a class="i_print"  href="javaScript:window.print();" title="Print it"></a>
                    </div>
                </div>
                <?php if (has_category('podcast', $post->ID)) { ?>
                    <?php if ($top_soundcloud = get_field('top_soundcloud')) { ?>
                        <div class="single_post__thumb full_frame">
                            <?php echo $top_soundcloud; ?>
                        </div>
                    <?php } else { ?>
                        <figure class="single_post__thumb">
                            <?php if (has_post_thumbnail()) : ?>
                                <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID))); ?>"
                                     alt="<?php the_title(); ?>">
                                <?php if (get_the_post_thumbnail_caption()) : ?>
                                    <figcaption>
                                        <?php the_post_thumbnail_caption(); ?>
                                    </figcaption>
                                <?php endif; ?>
                            <?php else : ?>
                                <img src="<?php echo esc_url(theme() . '/images/placeholder-dark.png'); ?>"
                                     alt="<?php the_title(); ?>">
                            <?php endif; ?>
                        </figure>
                    <?php } ?>
                <?php } else { ?>
                    <figure class="single_post__thumb">
                        <?php if (has_post_thumbnail()) : ?>
                            <img src="<?php echo esc_url(image_src(get_post_thumbnail_id($post->ID))); ?>"
                                 alt="<?php the_title(); ?>">
                            <?php if (get_the_post_thumbnail_caption()) : ?>
                                <figcaption>
                                    <?php the_post_thumbnail_caption(); ?>
                                </figcaption>
                            <?php endif; ?>
                        <?php else : ?>
                            <img src="<?php echo esc_url(theme() . '/images/placeholder-dark.png'); ?>"
                                 alt="<?php the_title(); ?>">
                        <?php endif; ?>
                    </figure>
                <?php } ?>
                <div class="single_post__content">
                    <div class="content">
                        <?php the_content(); ?>
                    </div>

                    <?php
                    $tag_taxs = get_the_terms($post->ID, 'post_tag');
                    if (!empty($tag_taxs) && !is_wp_error($tag_taxs)) : ?>
                        <div class="single_post__tags">
                            <strong>TAGS:</strong>
                            <?php echo wp_kses_post(custom_tax($post->ID, 'post_tag')); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>
<?php } ?>
<div class="single_post__latest_related">
    <div class="container flex_start__mob">
        <?php
        $lat_args = array(
            'posts_per_page' => 3,
            'post_type' => 'post',
            'post_status' => 'publish',
            'post__not_in' => array($post->ID)
        );

        $lat_posts = new WP_Query($lat_args);

        if ($lat_posts->have_posts()) : ?>
            <div class="single_post__latest">
                <div class="home_writers_title flex">
                    <h2>Latest</h2>
                    <a href="<?php echo esc_url(get_permalink(BLOG_ID)); ?>">See all</a>
                </div>
                <div class="single_post__lr_wrap">
                    <?php while ($lat_posts->have_posts()) : $lat_posts->the_post(); ?>
                        <div class="single_post__latest_item">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <time datetime="<?php echo get_the_date('Y-m-d'); ?>">
                                <?php echo get_the_date('F j, Y'); ?>
                            </time>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif;
        wp_reset_query(); ?>

        <?php if ($read_more = get_field('read_more')) {
            $cat_terms = get_the_terms($post->ID, 'category');
            if (!empty($cat_terms)) :
                $cat_term = $cat_terms[0];
            endif;
            ?>
            <div class="single_post__related">
                <div class="home_writers_title flex">
                    <h2>Read More</h2>
                    <a href="<?php echo esc_url(site_url($cat_term->slug)); ?>">See all</a>
                </div>
                <div class="single_post__lr_wrap">
                    <?php foreach ($read_more as $key => $value) {
                        $pid = $value; ?>
                        <div class="single_post__related_item">
                            <h4><a href="<?php echo get_the_permalink($pid); ?>"><?php echo get_the_title($pid); ?></a>
                            </h4>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } else {
            $cat_terms = get_the_terms($post->ID, 'category');
            if (!empty($cat_terms)) :
                $cat_term = $cat_terms[0];
                $term_id = $cat_term->term_id;
            endif;
            $rel_args = array(
                'posts_per_page' => 3,
                'post_type' => 'post',
                'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => array($term_id),
                        'operator' => 'IN'
                    ),
                ),
                'post__not_in' => array($post->ID)
            );

            $rel_posts = new WP_Query($rel_args);

            if ($rel_posts->have_posts()) : ?>
                <div class="single_post__related">
                    <div class="home_writers_title flex">
                        <?php if (in_category( 'podcast', $post->ID)) { ?>
                            <h2>More Podcasts</h2>
                        <?php } else { ?>
                            <h2>Read More</h2>
                        <?php } ?>
                        <a href="<?php echo esc_url(site_url($cat_term->slug)); ?>">See all</a>
                    </div>
                    <div class="single_post__lr_wrap">
                        <?php while ($rel_posts->have_posts()) : $rel_posts->the_post(); ?>
                            <div class="single_post__related_item">
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            <?php endif;
            wp_reset_query();
        } ?>
    </div>
</div>
<?php get_template_part('tpl-parts/sign-up-box') ?>

<?php get_footer(); ?>
