<?php get_header(); ?>
    <h1 hidden><?php the_title(); ?></h1>
    <div class="home_main_posts">
        <div class="container is_large flex_start">
			<?php if ( $left_column = get_field( 'left_column' ) ): ?>
                <div class="left_column posts_column">
					<?php foreach ( $left_column as $post ):
						// Setup this post for WP functions (variable must be named $post).
						setup_postdata( $post ); ?>
                        <div class="left_column_post home_post">
                            <a href="<?php the_permalink(); ?>" class="thumb">
								<?php if ( has_post_thumbnail() ) { ?>
                                    <picture>
                                        <source media="(max-width: 1024px)"
                                                srcset="<?php echo esc_url( image_src( get_post_thumbnail_id( $post->ID ), 'large' ) ); ?>">
                                        <img src="<?php echo esc_url( image_src( get_post_thumbnail_id(), 'full' ) ); ?>"
                                             alt="<?php the_title() ?>">
                                    </picture>
								<?php } else { ?>
                                    <img src="<?php echo esc_url( theme() . '/images/placeholder-dark.png' ); ?>"
                                         alt="<?php the_title() ?>">
								<?php } ?>
                            </a>
							<?php get_template_part( 'tpl-parts/cat-tax-group' ); ?>
                            <h3>
                                <a href="<?php the_permalink(); ?>"><?php echo get_field( 'alt_title' ) ? esc_html( get_field( 'alt_title' ) ) : the_title(); ?></a>
                            </h3>
							<?php get_template_part( 'tpl-parts/post-authors' ) ?>
                        </div>
					<?php endforeach; ?>
                </div>
				<?php
				// Reset the global post object so that the rest of the page works correctly.
				wp_reset_postdata(); ?>
			<?php endif; ?>

			<?php if ( $main_post = get_field( 'main_post' ) ): ?>
				<?php foreach ( $main_post as $post ):
					// Setup this post for WP functions (variable must be named $post).
					setup_postdata( $post );
					$content_post = get_post( $post->ID );
					$content      = $content_post->post_content;
					$content      = apply_filters( 'the_content', $content );
					$content      = str_replace( ']]>', ']]&gt;', $content );
					?>
                    <div class="main_post home_post">
                        <a href="<?php the_permalink(); ?>" class="thumb">
							<?php if ( has_post_thumbnail() ) { ?>
                                <picture>
                                    <source media="(max-width: 1024px)"
                                            srcset="<?php echo image_src( get_post_thumbnail_id(), 'large' ); ?>">
                                    <img src="<?php echo esc_url( image_src( get_post_thumbnail_id(), 'full' ) ); ?>"
                                         alt="<?php the_title(); ?>">
                                </picture>
							<?php } else { ?>
                                <img src="<?php echo esc_url( theme() . '/images/placeholder-dark.png' ); ?>"
                                     alt="<?php the_title(); ?>">
							<?php } ?>
                        </a>
                        <div class="cat_tax_wrap">
							<?php
							$cat_taxs = get_the_terms( $post->ID, 'category' );
							if ( ! empty( $cat_taxs ) && ! is_wp_error( $cat_taxs ) ) : ?>
                                <div class="single_post__cats">
									<?php echo wp_kses_post( full_cats( $post->ID ) ); ?>
                                </div>
							<?php endif; ?>
							<?php
							$photo_taxs = get_the_terms( $post->ID, 'photo_essay_cat' );
							if ( ! empty( $photo_taxs ) && ! is_wp_error( $photo_taxs ) ) : ?>
                                <div class="single_post__cats">
									<?php echo wp_kses_post( custom_tax( $post->ID, 'photo_essay_cat' ) ); ?>
                                </div>
							<?php endif; ?>
							<?php
							$analysis_taxs = get_the_terms( $post->ID, 'analysis' );
							if ( ! empty( $analysis_taxs ) && ! is_wp_error( $analysis_taxs ) ) : ?>
                                <div class="single_post__tax">
									<?php echo wp_kses_post( custom_tax( $post->ID, 'analysis' ) ); ?>
                                </div>
							<?php endif; ?>
                        </div>
                        <h2>
                            <a href="<?php the_permalink(); ?>"><?php echo get_field( 'alt_title' ) ? esc_html( get_field( 'alt_title' ) ) : the_title(); ?></a>
                        </h2>
                        <p><?php echo has_excerpt() ? wp_kses_post( get_the_excerpt() ) : wp_kses_post( wp_trim_words( $content, 30 ) ); ?></p>
						<?php get_template_part( 'tpl-parts/post-authors' ) ?>
                    </div>
				<?php endforeach; ?>
				<?php
				// Reset the global post object so that the rest of the page works correctly.
				wp_reset_postdata(); ?>
			<?php endif; ?>

			<?php if ( $right_column = get_field( 'right_column' ) ): ?>
                <div class="right_column posts_column">
					<?php foreach ( $right_column as $post ):
						// Setup this post for WP functions (variable must be named $post).
						setup_postdata( $post ); ?>
                        <div class="right_column_post home_post">
                            <a href="<?php the_permalink(); ?>" class="thumb">
								<?php if ( has_post_thumbnail() ) { ?>
                                    <img src="<?php echo image_src( get_post_thumbnail_id(), 'full' ); ?>"
                                         alt="<?php the_title(); ?>">
								<?php } else { ?>
                                    <img src="<?php echo esc_url( theme() . '/images/placeholder-dark.png' ); ?>"
                                         alt="<?php the_title() ?>">
								<?php } ?>
                            </a>
                            <div>
								<?php get_template_part( 'tpl-parts/cat-tax-group' ); ?>
                                <h4>
                                    <a href="<?php the_permalink(); ?>"><?php echo get_field( 'alt_title' ) ? esc_html( get_field( 'alt_title' ) ) : the_title(); ?></a>
                                </h4>
								<?php /*get_template_part('tpl-parts/post-authors') */
								?>
								<?php if ( $authors = get_field( 'authors' ) ) : ?>
                                    <div class="post__authors">
										<?php
										$a_c = count( $authors );
										$a_i = 1;
										foreach ( $authors as $key => $value ) :
											$status = get_post_status( $value ); ?>
											<?php if ( $key == 0 ) { ?>
                                            <div class="post__author">
												<?php if ( $status === 'publish' ) { ?>
                                                    <a href="<?php echo esc_url( get_permalink( $value ) ); ?>">
														<?php
														echo esc_html( get_the_title( $value ) );
														/*echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';*/
														?>
                                                    </a>
												<?php } elseif ( $status === 'private' ) { ?>
                                                    <span>
                                                <?php
                                                echo str_replace( 'Private: ', '', get_the_title( $value ) );
                                                /*  echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';*/
                                                ?>
                                            </span>
												<? } ?>
                                            </div>
										<?php } ?>
										<?php endforeach; ?>
                                    </div>
								<?php endif; ?>
                            </div>
                        </div>
					<?php endforeach; ?>
                </div>
				<?php
				// Reset the global post object so that the rest of the page works correctly.
				wp_reset_postdata(); ?>
			<?php endif; ?>
        </div>
    </div>

<?php if ( $posts_row = get_field( 'posts_row' ) ): ?>
    <div class="home_posts_row">
        <div class="container is_large">
            <div class="flex_start">
				<?php foreach ( $posts_row as $post ):
					// Setup this post for WP functions (variable must be named $post).
					setup_postdata( $post );
					$content_post = get_post( $post->ID );
					$content      = $content_post->post_content;
					$content      = apply_filters( 'the_content', $content );
					$content      = str_replace( ']]>', ']]&gt;', $content );
					?>
                    <div class="home_posts_row__post_item home_post">
                        <a href="<?php echo esc_url( get_the_permalink() ) ?>" class="thumb">
							<?php if ( has_post_thumbnail() ) { ?>
                                <picture>
                                    <source media="(max-width: 1024px)"
                                            srcset="<?php echo image_src( get_post_thumbnail_id( $post->ID ), 'medium_large' ); ?>">
                                    <img src="<?php echo esc_url( image_src( get_post_thumbnail_id(), 'full' ) ); ?>"
                                         alt="<?php echo get_the_title(); ?>">
                                </picture>
							<?php } else { ?>
                                <img src="<?php echo esc_url( theme() . '/images/placeholder-dark.png' ); ?>"
                                     alt="<?php the_title(); ?>">
							<?php } ?>
                        </a>
						<?php get_template_part( 'tpl-parts/cat-tax-group' ); ?>
                        <h4>
                            <a href="<?php echo esc_url( get_the_permalink() ) ?>"><?php echo get_field( 'alt_title' ) ? esc_html( get_field( 'alt_title' ) ) : get_the_title(); ?></a>
                        </h4>
                        <!--<p><?php /*echo has_excerpt() ? wp_kses_post( get_the_excerpt() ) : wp_kses_post( wp_trim_words( $content, 15 ) ); */
						?></p>-->
						<?php /*echo get_field( 'sub_title' ) ? '<p>'. esc_html(get_field( 'sub_title' )) .'</p>' : ''; */
						?>
						<?php get_template_part( 'tpl-parts/post-authors' ) ?>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </div>
	<?php
	// Reset the global post object so that the rest of the page works correctly.
	wp_reset_postdata(); ?>
<?php endif; ?>

<?php if ( $podcasts = get_field( 'podcasts' ) ) { ?>
    <section class="home_podcasts">
        <div class="container is_large">
            <div class="home_writers_title flex">
                <h2><?php echo esc_html( 'Podcasts' ) ?></h2>
                <a href="<?php echo esc_url( site_url() ); ?>/podcast/"><?php echo esc_html( 'See all' ) ?></a>
            </div>
            <div class="home_podcasts__wrapper flex__rwd">
				<?php foreach ( $podcasts as $key => $value ) {
					$pid = $value; ?>
                    <div class="home_podcasts__post flex">
						<?php if ( has_post_thumbnail( $pid ) ) { ?>
                            <figure>
                                <a href="<?php echo get_permalink( $pid ) ?>">
                                    <img src="<?php echo esc_url( image_src( get_post_thumbnail_id( $pid ), 'full' ) ); ?>"
                                         alt="<?php echo esc_attr( get_alt( $pid ) ); ?>">
                                </a>
                            </figure>
						<?php } ?>
                        <div class="home_podcasts__post_info">
                            <h3><a href="<?php echo get_permalink( $pid ) ?>"><?php echo get_the_title( $pid ); ?></a>
                            </h3>
                            <div class="flex">
                                <div class="cat_tax_wrap">
									<?php
/*									$cat_taxs = get_the_terms( $pid, 'category' );
									if ( ! empty( $cat_taxs ) && ! is_wp_error( $cat_taxs ) ) : */?><!--
                                        <div class="single_post__cats">
											<?php /*echo wp_kses_post( custom_tax( $pid, 'category' ) ); */?>
                                        </div>
									--><?php /*endif; */?>
	                                <?php
	                                $analysis_taxs = get_the_terms( $pid, 'analysis' );
	                                if ( ! empty( $analysis_taxs ) && ! is_wp_error( $analysis_taxs ) ) : ?>
                                        <div class="single_post__tax">
			                                <?php echo wp_kses_post( custom_tax( $pid, 'analysis' ) ); ?>
                                        </div>
	                                <?php endif; ?>
                                </div>
                                <a href="<?php echo get_permalink( $pid ) ?>" class="i_play_circle_o"></a>
                            </div>
                        </div>
                    </div>
				<?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php if ( $posts_row = get_field( 'posts_row_2' ) ): ?>
    <div class="home_posts_row">
        <div class="container is_large">
            <div class="flex_start">
				<?php foreach ( $posts_row as $post ):
					// Setup this post for WP functions (variable must be named $post).
					setup_postdata( $post );
					$content_post = get_post( $post->ID );
					$content      = $content_post->post_content;
					$content      = apply_filters( 'the_content', $content );
					$content      = str_replace( ']]>', ']]&gt;', $content );
					?>
                    <div class="home_posts_row__post_item home_post">
                        <a href="<?php echo esc_url( get_the_permalink() ) ?>" class="thumb">
							<?php if ( has_post_thumbnail() ) { ?>
                                <picture>
                                    <source media="(max-width: 1024px)"
                                            srcset="<?php echo image_src( get_post_thumbnail_id( $post->ID ), 'medium_large' ); ?>">
                                    <img src="<?php echo esc_url( image_src( get_post_thumbnail_id(), 'full' ) ); ?>"
                                         alt="<?php echo get_the_title(); ?>">
                                </picture>
							<?php } else { ?>
                                <img src="<?php echo esc_url( theme() . '/images/placeholder-dark.png' ); ?>"
                                     alt="<?php the_title(); ?>">
							<?php } ?>
                        </a>
						<?php get_template_part( 'tpl-parts/cat-tax-group' ); ?>
                        <h4>
                            <a href="<?php echo esc_url( get_the_permalink() ) ?>"><?php echo get_field( 'alt_title' ) ? esc_html( get_field( 'alt_title' ) ) : get_the_title(); ?></a>
                        </h4>
                        <!--<p><?php /*echo has_excerpt() ? wp_kses_post( get_the_excerpt() ) : wp_kses_post( wp_trim_words( $content, 15 ) ); */
						?></p>-->
						<?php /*echo get_field( 'sub_title' ) ? '<p>'. esc_html(get_field( 'sub_title' )) .'</p>' : ''; */
						?>
						<?php get_template_part( 'tpl-parts/post-authors' ) ?>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </div>
	<?php
	// Reset the global post object so that the rest of the page works correctly.
	wp_reset_postdata(); ?>
<?php endif; ?>

<?php if ( $editors_picks = get_field( 'editors_picks' ) ) { ?>
    <section class="home_editors_picks">
        <div class="container is_large">
            <div class="home_writers_title flex">
                <h2><?php echo esc_html( "Editors' Picks" ) ?></h2>
            </div>
            <div class="home_editors_picks__wrapper flex">
				<?php foreach ( $editors_picks as $key => $value ) {
					$pid = $value; ?>
                    <a href="<?php echo get_permalink( $pid ); ?>" class="home_editors_picks__post">
						<?php if ( has_post_thumbnail( $pid ) ) { ?>
                            <img src="<?php echo esc_url( image_src( get_post_thumbnail_id( $pid ), 'medium_large' ) ); ?>"
                                 alt="<?php echo esc_attr( get_alt( $pid ) ); ?>">
						<?php } else { ?>
                            <img src="<?php echo esc_url( theme( 'images/placeholder-dark.png' ) ) ?>"
                                 alt="<?php echo esc_attr( get_alt( $pid ) ); ?>">
						<?php } ?>
                        <div class="home_editors_picks__post_info">
                            <h3 class="h4"><?php echo get_the_title( $pid ); ?></h3>
							<?php if ( $authors = get_field( 'authors', $pid ) ) : ?>
                                <div class="post_authors">
									<?php
									$a_c = count( $authors );
									$a_i = 1;
									foreach ( $authors as $key => $value ) : ?>
                                        <small>
											<?php
											echo esc_html( get_the_title( $value ) );
											echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
											?>
                                        </small>
									<?php endforeach; ?>
                                </div>
							<?php endif; ?>
                        </div>
                    </a>
				<?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php if ( get_field( 'hide_pe' ) != true ) { ?>
	<?php if ( $photo_essays = get_field( 'photo_essays' ) ): ?>
        <section class="home_photo_essay">
            <div class="container is_large">
                <div class="home_writers_title flex">
                    <h2><?php echo esc_html( 'Photo Essays' ) ?></h2>
                    <a href="/photo-essays/"><?php echo esc_html( 'See all' ) ?></a>
                </div>
                <div class="home_photo_essay_posts flex">
					<?php foreach ( $photo_essays as $post ):
						// Setup this post for WP functions (variable must be named $post).
						setup_postdata( $post ); ?>
                        <a href="<?php the_permalink(); ?>" class="photo_essay_post">
                            <img src="<?php echo esc_url( image_src( get_post_thumbnail_id( $post->ID ), 'photo_essay' ) ); ?>"
                                 alt="<?php echo esc_attr( get_alt( $post->ID ) ); ?>">
                            <div>
                                <h4><?php the_title(); ?></h4>
								<?php if ( $authors = get_field( 'authors' ) ) : ?>
                                    <div class="photo_essay__post_authors">
										<?php
										$a_c = count( $authors );
										$a_i = 1;
										foreach ( $authors as $key => $value ) : ?>
                                            <small>
												<?php
												echo esc_html( get_the_title( $value ) );
												echo ( $a_i ++ != $a_c ) ? '<span>,</span> ' : '';
												?>
                                            </small>
										<?php endforeach; ?>
                                    </div>
								<?php endif; ?>
                            </div>
                        </a>
					<?php endforeach; ?>
                </div>
            </div>
        </section>
		<?php
		// Reset the global post object so that the rest of the page works correctly.
		wp_reset_postdata(); ?>
	<?php endif; ?>
<?php } ?>

<?php
$args = array(
	'post_type' => 'post,photo_essay',
	'range'     => 'last24hours',
	'limit'     => 8
); ?>
<?php if ( function_exists( 'wpp_get_mostpopular' ) ) { ?>
    <section class="home_popular">
        <div class="container is_large">
            <div class="home_writers_title flex">
                <h2><?php echo esc_html( 'Popular' ) ?></h2>
                <a href="/latest/"><?php echo esc_html( 'See all' ) ?></a>
            </div>
            <div class="home_popular_posts">
				<?php wpp_get_mostpopular( $args ); ?>
            </div>
        </div>
    </section>
<?php } ?>
<?php if ( get_field( 'hide_writers_section' ) != true ) { ?>
	<?php if ( $authors = get_field( 'writers' ) ) : ?>
        <div class="home_writers">
            <div class="container is_large">
                <div class="home_writers_title flex">
                    <h2><?php echo esc_html( 'Writers' ) ?></h2>
                    <a href="/about/"><?php echo esc_html( 'See all' ) ?></a>
                </div>
                <div class="writers_wrapper flex">
					<?php foreach (
						$authors
						as $key => $value
					) :
						$status = get_post_status( $value ); ?>
						<?php if ( $status === 'publish' ) { ?>
                        <a href="<?php echo esc_url( get_permalink( $value ) ); ?>" class="post_writer flex">
                            <figure class="thumb">
								<?php if ( has_post_thumbnail( $value ) ) { ?>
                                    <img src="<?php echo image_src( get_post_thumbnail_id( $value ), 'people_tiny' ); ?>"
                                         alt="<?php echo get_the_title( $value ); ?>">
								<?php } else { ?>
                                    <img src="<?php echo esc_url( theme() . '/images/author-placeholder.png' ) ?>"
                                         alt="<?php the_title(); ?>">
								<?php } ?>
                            </figure>
                            <span><?php echo esc_html( get_the_title( $value ) ); ?></span>
                        </a>
					<?php } elseif ( $status === 'private' ) { ?>
                        <div class="post_writer flex">
                            <figure class="thumb">
								<?php if ( has_post_thumbnail( $value ) ) { ?>
                                    <img src="<?php echo image_src( get_post_thumbnail_id( $value ), 'people_tiny' ); ?>"
                                         alt="<?php echo get_the_title( $value ); ?>">
								<?php } else { ?>
                                    <img src="<?php echo esc_url( theme() . '/images/author-placeholder.png' ) ?>"
                                         alt="<?php the_title(); ?>">
								<?php } ?>
                            </figure>
                            <span><?php echo str_replace( 'Private: ', '', get_the_title( $value ) ); ?></span>
                        </div>
					<? } ?>
					<?php endforeach; ?>
                </div>
            </div>
        </div>
	<?php endif; ?>
<?php } ?>
<?php if ( $contributors = get_field( 'contributors' ) ) { ?>
    <section class="home_contributors">
        <div class="container is_large">
            <div class="home_writers_title flex">
                <h2><?php echo esc_html( 'Contributors' ) ?></h2>
            </div>
            <div class="home_contributors__wrapper flex">
				<?php foreach ( $contributors as $contributor ) { ?>
                    <div class="contributor flex_start">
						<?php if ( $contributor['image'] ) { ?>
                            <figure><img
                                        src="<?php echo esc_url( image_src( $contributor['image']['id'], 'thumbnail' ) ); ?>"
                                        alt="<?php echo esc_attr( $contributor['name'] ); ?>"></figure>
						<?php } ?>
                        <div class="contributor_info">
							<?php echo $contributor['name'] ? '<strong>' . esc_html( $contributor['name'] ) . '</strong>' : ''; ?>
							<?php if ( $contributor['link'] ) { ?>
									<?php echo $contributor['quote'] ? '<p><a href="'. esc_url( $contributor['link'] ).'">' . esc_html( $contributor['quote'] ) . '</a></p>' : ''; ?>
							<?php } else {
								echo $contributor['quote'] ? '<p>' . esc_html( $contributor['quote'] ) . '</p>' : '';
							} ?>
                        </div>
                    </div>
				<?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php get_footer(); ?>