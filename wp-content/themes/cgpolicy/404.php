<?php get_header(); ?>

<section class="default_content container left_spacing">
    <div class="default_content__inner">
        <div class="default_content__title">
            <h1>Error 404</h1>
        </div>
        <div class="content">
            <h4>Sorry, this page doesn't exist or has been removed.</h4>
            <a href="<?php echo esc_url(site_url()); ?>" class="button is_bigger">Go back home</a>
        </div>
    </div>
</section>

<?php get_footer(); ?>
